package beans;

import java.io.Serializable;

// 購入詳細
public class BuyDetailBeans implements Serializable {
  private int id;
  private int itemId;
  private int buyId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getItemId() {
    return itemId;
  }

  public void setItemId(int itemId) {
    this.itemId = itemId;
  }

  public int getBuyId() {
    return buyId;
  }

  public void setBuyId(int buyId) {
    this.buyId = buyId;
  }
}
