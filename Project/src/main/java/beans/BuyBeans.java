package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

// 購入データ
public class BuyBeans implements Serializable {
  private int buyId;
  private int userId;
  private int delivertMethodId;
  private int totalPrice;
  private int couponUserId;
  private Timestamp buyDate;
  private int useCouponDiscount;
  private int couponId;

  // 配送方法テーブルより
  private String deliveryMethodName;
  private int deliveryMethodPrice;

  // クーポンマスタテーブルより
  private String couponName;

  public int getBuyId() {
    return buyId;
  }

  public void setBuyId(int buyId) {
    this.buyId = buyId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getDelivertMethodId() {
    return delivertMethodId;
  }

  public void setDelivertMethodId(int delivertMethodId) {
    this.delivertMethodId = delivertMethodId;
  }

  public int getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(int totalPrice) {
    this.totalPrice = totalPrice;
  }

  public int getCouponUserId() {
    return couponUserId;
  }

  public void setCouponUserId(int couponUserId) {
    this.couponUserId = couponUserId;
  }

  public Timestamp getBuyDate() {
    return buyDate;
  }

  public void setBuyDate(Timestamp buyDate) {
    this.buyDate = buyDate;
  }

  public String getFormatBuyDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(buyDate);
  }

  public String getFormatTotalPrice() {
    return String.format("%,d", this.totalPrice);
  }

  public int getUseCouponDiscount() {
    return useCouponDiscount;
  }

  public void setUseCouponDiscount(int useCouponDiscount) {
    this.useCouponDiscount = useCouponDiscount;
  }

  public String getFormatUseCouponDiscount() {
    return String.format("%,d", this.useCouponDiscount);
  }

  public String getDeliveryMethodName() {
    return deliveryMethodName;
  }

  public void setDeliveryMethodName(String deliveryMethodName) {
    this.deliveryMethodName = deliveryMethodName;
  }

  public int getDeliveryMethodPrice() {
    return deliveryMethodPrice;
  }

  public void setDeliveryMethodPrice(int deliveryMethodPrice) {
    this.deliveryMethodPrice = deliveryMethodPrice;
  }

  public String getCouponName() {
    return couponName;
  }

  public void setCouponName(String couponName) {
    this.couponName = couponName;
  }

  public int getCouponId() {
    return couponId;
  }

  public void setCouponId(int couponId) {
    this.couponId = couponId;
  }
}
