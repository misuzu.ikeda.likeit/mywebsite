package beans;

import java.io.Serializable;

// 配送方法
public class DeliveryMethodBeans implements Serializable {
  private int deliveryMethodId;
  private String name;
  private int price;


  public int getDeliveryMethodId() {
    return deliveryMethodId;
  }

  public void setDeliveryMethodId(int deliveryMethodId) {
    this.deliveryMethodId = deliveryMethodId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
}
