package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

// ログイン回数データ
public class LoginCountBeans implements Serializable {
  private int loginCountId;
  private int userId;
  private Timestamp createDate;

  public int getLoginCountId() {
    return loginCountId;
  }

  public void setLoginCountId(int loginCountId) {
    this.loginCountId = loginCountId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public String getFormatDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(createDate);
  }
}
