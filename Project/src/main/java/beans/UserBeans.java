package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

// ユーザー
public class UserBeans implements Serializable {
  private int userId;
  private String userName;
  private boolean adminFlag;
  private String postCode;
  private String address;
  private Date birthDate;
  private String loginId;
  private String password;
  private Timestamp createDate;
  private Timestamp updateDate;

  public UserBeans() {

  }

  // ログインセッションを保存するためのコンストラクタ
  public UserBeans(String loginId, String password, boolean adminFlag) {
    this.loginId = loginId;
    this.password = password;
    this.adminFlag = adminFlag;
  }

  // すべてのデータをセットするコンストラクタ
  public UserBeans(int userId, String userName, boolean adminFlag, String postCode, String address,
      Date birthDate, String loginId, String password, Timestamp createDate, Timestamp updateDate) {
    super();
    this.userId = userId;
    this.userName = userName;
    this.adminFlag = adminFlag;
    this.postCode = postCode;
    this.address = address;
    this.birthDate = birthDate;
    this.loginId = loginId;
    this.password = password;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }



  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public boolean isAdminFlag() {
    return adminFlag;
  }

  public void setAdminFlag(boolean adminFlag) {
    this.adminFlag = adminFlag;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }
}
