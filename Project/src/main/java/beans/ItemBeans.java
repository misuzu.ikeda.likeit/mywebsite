package beans;

import java.io.Serializable;

// 商品データ
public class ItemBeans implements Serializable {
  private int itemId;
  private String itemName;
  private String itemDetail;
  private int itemPrice;
  private String itemImage;

  public ItemBeans() {

  }

  public ItemBeans(int itemId, String itemName, String itemDetail, int itemPrice,
      String itemImage) {
    super();
    this.itemId = itemId;
    this.itemName = itemName;
    this.itemDetail = itemDetail;
    this.itemPrice = itemPrice;
    this.itemImage = itemImage;
  }

  public int getItemId() {
    return itemId;
  }

  public void setItemId(int itemId) {
    this.itemId = itemId;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemDetail() {
    return itemDetail;
  }

  public void setItemDetail(String itemDetail) {
    this.itemDetail = itemDetail;
  }

  public int getItemPrice() {
    return itemPrice;
  }

  public void setItemPrice(int itemPrice) {
    this.itemPrice = itemPrice;
  }

  public String getItemImage() {
    return itemImage;
  }

  public void setItemImage(String itemImage) {
    this.itemImage = itemImage;
  }

  public String getFormatPrice() {
    return String.format("%,d", this.itemPrice);
  }
}
