package beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

// ユーザーが取得しているクーポンデータ
public class CouponUserBeans implements Serializable {
  private int couponUserId;
  private int userId;
  private int couponId;
  private Timestamp couponGetDate;

  private int couponCnt;
  private String couponName;

  public CouponUserBeans() {

  }

  // クーポン枚数を取得するためのコンストラクタ
  public CouponUserBeans(int couponCnt, String couponName) {
    this.couponCnt = couponCnt;
    this.couponName = couponName;
  }

  public int getCouponUserId() {
    return couponUserId;
  }

  public void setCouponUserId(int couponUserId) {
    this.couponUserId = couponUserId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getCouponId() {
    return couponId;
  }

  public void setCouponId(int couponId) {
    this.couponId = couponId;
  }

  public Timestamp getCouponGetDate() {
    return couponGetDate;
  }

  public void setCouponGetDate(Timestamp couponGetDate) {
    this.couponGetDate = couponGetDate;
  }

  public String getFormatDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(couponGetDate);
  }

  public int getCouponCnt() {
    return couponCnt;
  }

  public void setCouponCnt(int couponCnt) {
    this.couponCnt = couponCnt;
  }

  public String getCouponName() {
    return couponName;
  }

  public void setCouponName(String couponName) {
    this.couponName = couponName;
  }

}
