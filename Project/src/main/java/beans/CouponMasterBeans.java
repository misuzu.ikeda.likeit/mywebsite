package beans;

import java.io.Serializable;
import java.sql.Timestamp;

// クーポンマスタ
public class CouponMasterBeans implements Serializable {
  private int couponId;
  private String couponName;
  private Timestamp couponLimit;
  private int discountYen;
  private int discountPercent;
  private boolean freeShipping;
  private String couponCode;

  // クーポンユーザーテーブルより
  private int couponUserId;

  public int getCouponId() {
    return couponId;
  }

  public void setCouponId(int couponId) {
    this.couponId = couponId;
  }

  public String getCouponName() {
    return couponName;
  }

  public void setCouponName(String couponName) {
    this.couponName = couponName;
  }

  public Timestamp getCouponLimit() {
    return couponLimit;
  }

  public void setCouponLimit(Timestamp couponLimit) {
    this.couponLimit = couponLimit;
  }

  public int getDiscountYen() {
    return discountYen;
  }

  public void setDiscountYen(int discountYen) {
    this.discountYen = discountYen;
  }

  public int getDiscountPercent() {
    return discountPercent;
  }

  public void setDiscountPercent(int discountPercent) {
    this.discountPercent = discountPercent;
  }

  public boolean isFreeShipping() {
    return freeShipping;
  }

  public void setFreeShipping(boolean freeShipping) {
    this.freeShipping = freeShipping;
  }

  public String getCouponCode() {
    return couponCode;
  }

  public void setCouponCode(String couponCode) {
    this.couponCode = couponCode;
  }

  public int getCouponUserId() {
    return couponUserId;
  }

  public void setCouponUserId(int couponUserId) {
    this.couponUserId = couponUserId;
  }
}
