package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;

/**
 * クーポンくじページ
 */
@WebServlet("/CouponDrawing")
public class CouponDrawing extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CouponDrawing() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    request.getRequestDispatcher(EcHelper.COUPON_KUJI_PAGE).forward(request, response);

  }

}
