package ec;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import dao.ItemDAO;

/**
 * 商品をカートに追加
 */
@WebServlet("/CartAdd")
public class CartAdd extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CartAdd() {
    super();
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      // 選択された商品のIDを型変換し利用
      int id = Integer.parseInt(request.getParameter("item_id"));
      // 対象のアイテム情報を取得
      ItemBeans item = ItemDAO.getItemByItemID(id);

      // 追加した商品を表示するためリクエストパラメーターにセット
      request.setAttribute("item", item);

      // カートを取得
      ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

      // セッションにカートがない場合カートを作成
      if (cart == null) {
        cart = new ArrayList<ItemBeans>();
      }
      // カートに商品を追加。
      cart.add(item);
      // カート情報更新
      session.setAttribute("cart", cart);
      request.setAttribute("cartActionMessage", "商品を追加しました");
      request.getRequestDispatcher(EcHelper.CART_PAGE).forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
