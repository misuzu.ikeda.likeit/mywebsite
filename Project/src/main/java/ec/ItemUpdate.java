package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDAO;

/**
 * 商品マスタ更新画面（※管理者専用）
 */
@WebServlet("/ItemUpdate")
public class ItemUpdate extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ItemUpdate() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    try {

      // URLパラメータからidを取得
      int id = Integer.valueOf(request.getParameter("item_id"));

      // IDに紐づく情報を取得
      ItemBeans item = ItemDAO.getItemByItemID(id);
      request.setAttribute("item", item);

      request.getRequestDispatcher(EcHelper.ITEM_UPDATE_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      ItemBeans item = new ItemBeans();
      ItemDAO itemD = new ItemDAO();

      // URLパラメータからidを取得
      int id = Integer.valueOf(request.getParameter("id"));

      // 更新画面に入力された情報を取得
      String itemName = request.getParameter("item_name");
      String itemDetail = request.getParameter("item_detail");
      String itemPrice = request.getParameter("item_price");
      String itemImage = request.getParameter("item_image");

      // 商品名が重複している場合
      if (ItemDAO.existItemName(itemName)) {
        // 入力した値を再度表示するために値をセットする

        int price = Integer.valueOf(request.getParameter("item_price"));

        item.setItemName(itemName);
        item.setItemDetail(itemDetail);
        item.setItemPrice(price);
        item.setItemImage(itemImage);

        request.setAttribute("item", item);

        request.setAttribute("errMsg", "商品名とほかの商品と重複しています");

        request.getRequestDispatcher(EcHelper.ITEM_UPDATE_PAGE).forward(request, response);
        return;
      }

      // 更新処理
      itemD.updateItem(id, itemName, itemDetail, itemPrice, itemImage);

      response.sendRedirect("ItemList");

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
