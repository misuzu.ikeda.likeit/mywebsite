package ec;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyBeans;
import beans.ItemBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class BuyHistoryDetail
 */
@WebServlet("/BuyHistoryDetail")
public class BuyHistoryDetail extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public BuyHistoryDetail() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      // URLからGETパラメータとしてIDを受け取る
      String bId = request.getParameter("buy_id");
      int buyId = Integer.parseInt(bId);

      // buyIdを引数にして、紐づく購入履歴情報を取得
      BuyBeans buyHistory = BuyDAO.getBuyBeansByBuyId(buyId);

      int totalPrice = buyHistory.getTotalPrice();

      double doubleTaxPrice =
          totalPrice - ((int) ((totalPrice) - buyHistory.getDeliveryMethodPrice()) / 1.1)
              - buyHistory.getDeliveryMethodPrice();

      int taxPrice = (int) Math.round(doubleTaxPrice);

      ArrayList<ItemBeans> buyDetailItemList = BuyDetailDAO.getItemBeansListByBuyId(buyId);

      request.setAttribute("buyHistory", buyHistory);
      request.setAttribute("buyDetailTaxPrice", taxPrice);
      request.setAttribute("buyDetailItem", buyDetailItemList);

      request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
