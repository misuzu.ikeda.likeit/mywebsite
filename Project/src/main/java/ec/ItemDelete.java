package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDAO;

/**
 * 商品マスタ削除画面（※管理者専用）
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ItemDelete() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }



    try {
      // URLパラメータからidを取得
      int id = Integer.valueOf(request.getParameter("item_id"));

      new ItemDAO();
      // idに紐づくユーザー情報を取得
      ItemBeans item = ItemDAO.getItemByItemID(id);
      request.setAttribute("item", item);

      request.getRequestDispatcher(EcHelper.ITEM_DELETE_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    // idを取得
    int id = Integer.valueOf(request.getParameter("id"));

    try {
      ItemDAO item = new ItemDAO();
      // 削除処理
      item.deleteItem(id);

      response.sendRedirect("ItemList");

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
