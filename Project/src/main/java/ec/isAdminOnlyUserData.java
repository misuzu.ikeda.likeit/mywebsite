package ec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyBeans;
import beans.CouponUserBeans;
import beans.UserBeans;
import dao.BuyDAO;
import dao.CouponUserDAO;
import dao.LoginCountDAO;
import dao.UserDAO;

/**
 * ユーザー情報の参照と更新画面（※管理者専用ページ）
 */
@WebServlet("/isAdminOnryUserData")
public class isAdminOnlyUserData extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public isAdminOnlyUserData() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    try {
      UserBeans user = new UserBeans();
      UserDAO ud = new UserDAO();

      // URLパラメータからidを取得
      int id = Integer.valueOf(request.getParameter("id"));

      // IDに紐づく情報を取得
      UserBeans userData = ud.findById(id);
      request.setAttribute("userData", userData);

      // 購入履歴を取得
      BuyDAO bDao = new BuyDAO();
      ArrayList<BuyBeans> buyDataList = bDao.getBuyList(id);

      request.setAttribute("buyDataList", buyDataList);

      // ログイン回数の取得
      double loginCount = LoginCountDAO.getLoginCount(id);
      request.setAttribute("loginCount", (int) loginCount);

      // クーポン枚数の取得
      CouponUserDAO cud = new CouponUserDAO();
      List<CouponUserBeans> couponCntList = new ArrayList<CouponUserBeans>();
      couponCntList = cud.getCountCouponByUserId(id);
      request.setAttribute("couponCountList", couponCntList);

      request.getRequestDispatcher(EcHelper.USER_INFO_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      UserBeans user = new UserBeans();
      UserDAO ud = new UserDAO();

      // URLパラメータからidを取得
      int id = Integer.valueOf(request.getParameter("id"));

      // 更新画面に入力された情報を取得
      String userName = request.getParameter("user_name");
      String postCode = request.getParameter("post_code");
      String address = request.getParameter("address");
      String password = request.getParameter("password");
      String confirmPassword = request.getParameter("confirm_password");

      // passwordが間違っている場合
      if (!password.equals(confirmPassword)) {
        // エラーメッセージと更新画面へ表示する情報をセットする
        request.setAttribute("errMsg", "入力されているパスワードと確認用パスワードが違います");

        // 入力した値を再度表示するために値をセットする
        user.setUserName(userName);
        user.setPostCode(postCode);
        user.setAddress(address);

        request.setAttribute("userData", user);

        // 更新画面へ
        request.getRequestDispatcher(EcHelper.USER_INFO_PAGE).forward(request, response);
        return;
      }

      // password 更新なしの場合
      if (password.equals("") && confirmPassword.equals("")) {
        ud.updateUserExcludePassword(id, userName, postCode, address);

      } else {
        // password 更新ありの場合
        ud.updateUser(id, userName, postCode, address, password);
      }

      // 更新完了画面へ遷移
      response.sendRedirect("UserUpdateResult");

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
