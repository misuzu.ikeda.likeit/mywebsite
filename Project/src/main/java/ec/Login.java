package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.LoginCountDAO;
import dao.UserDAO;

/**
 * ログイン画面
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Login() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    // ログイン失敗時の処理
    String inputLoginId = session.getAttribute("loginId") != null
        ? (String) EcHelper.cutSessionAttribute(session, "loginId")
        : "";
    String loginErrorMessage = (String) EcHelper.cutSessionAttribute(session, "loginErrorMessage");

    request.setAttribute("inputLoginId", inputLoginId);
    request.setAttribute("loginErrorMessage", loginErrorMessage);

    request.getRequestDispatcher(EcHelper.LOGIN_PAGE).forward(request, response);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    try {
      // パラメーターから取得
      String loginId = request.getParameter("login_id");
      String password = request.getParameter("password");

      UserBeans loginInfo = UserDAO.getLoginInfo(loginId, password);

      // ログイン情報が取得できた場合
      if (loginInfo != null) {
        session.setAttribute("isLogin", true);
        session.setAttribute("loginInfo", loginInfo);
        // ログイン前のページを取得
        String returnStrUrl = (String) EcHelper.cutSessionAttribute(session, "returnStrUrl");

        // ユーザーIDを取得
        int userId = UserDAO.getUserId(loginId, password);

        // ユーザーIDが取得できた場合
        if (userId != 0) {
          session.setAttribute("userId", userId);
        }

        // ログイン回数を追加
        LoginCountDAO lc = new LoginCountDAO();
        lc.insertLoginCount(userId);

        // ログイン前ページにリダイレクト 指定がない場合はTOPページへ
        response.sendRedirect(returnStrUrl != null ? returnStrUrl : "Top");
      } else {
        session.setAttribute("loginId", loginId);
        session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
        response.sendRedirect("Login");
      }
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
