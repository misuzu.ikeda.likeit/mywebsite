package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.ItemDAO;

/**
 * 商品マスタ登録画面（※管理者専用）
 */
@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ItemAdd() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    request.getRequestDispatcher(EcHelper.ITEM_ADD_PAGE).forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      ItemDAO itemD = new ItemDAO();

      String itemName = request.getParameter("item_name");
      String itemDetail = request.getParameter("item_detail");
      String itemPrice = request.getParameter("item_price");
      String itemImage = request.getParameter("item_image");

      // 商品名が重複している場合
      if (ItemDAO.existItemName(itemName)) {
        // 入力内容を画面に表示するために、リクエストスコープに値をセットしておく
        request.setAttribute("inputItemName", itemName);
        request.setAttribute("inputItemDetail", itemDetail);
        request.setAttribute("inputItemPrice", itemPrice);
        request.setAttribute("inputItemImage", itemImage);

        request.setAttribute("errMsg", "ほかの商品名と重複しています");

        request.getRequestDispatcher(EcHelper.ITEM_ADD_PAGE).forward(request, response);
        return;
      }

      // 登録処理
      itemD.insertItem(itemName, itemDetail, itemPrice, itemImage);

      response.sendRedirect("ItemList");

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
