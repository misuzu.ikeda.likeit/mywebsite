package ec;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UserDAO;

/**
 * ユーザーマスタ一覧画面（※管理者専用）
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserList() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    try {
      // ユーザ一覧情報を取得
      UserDAO ud = new UserDAO();
      List<UserBeans> userList = ud.findAllUserData();

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);

      request.getRequestDispatcher(EcHelper.USER_LIST_PAGE).forward(request, response);

      // TODO ページング処理

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      UserDAO ud = new UserDAO();

      // 検索条件取得
      String loginId = request.getParameter("user_loginId");
      String userName = request.getParameter("user_name");
      String address = request.getParameter("address");
      String startDate = request.getParameter("date_start");
      String endDate = request.getParameter("date_end");

      // 検索処理
      List<UserBeans> userList = ud.searchUser(loginId, userName, address, startDate, endDate);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", userName);
      request.setAttribute("address", address);
      request.setAttribute("startDate", startDate);
      request.setAttribute("endDate", endDate);

      // ユーザー一覧画面へ遷移
      request.getRequestDispatcher(EcHelper.USER_LIST_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
