package ec;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDAO;
import dao.LoginCountDAO;

/**
 * TOP画面（ホーム画面）
 */
@WebServlet("/Top")
public class Top extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Top() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    try {
      UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

      // 商品情報を取得
      ArrayList<ItemBeans> itemList = ItemDAO.getRandItem(4);

      // リクエストスコープにセット
      request.setAttribute("itemList", itemList);

      // セッションに検索ワードが入っていたら破棄する
      String searchWord = (String) session.getAttribute("searchWord");
      if (searchWord != null) {
        session.removeAttribute("searchWord");
      }

      // ログイン中のユーザーが10回ログインくじ未抽選の場合
      String kujiDone = (String) session.getAttribute("kujiDone");

      if (loginCheck != null && kujiDone == null) {
        int id = (int) session.getAttribute("userId");
        double loginCount = LoginCountDAO.getLoginCount(id);
        request.setAttribute("loginCount", (int) loginCount);
      }

      request.getRequestDispatcher(EcHelper.TOP_PAGE).forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
