package ec;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import beans.ItemBeans;

// 定数保持、処理及び表示簡略化のためのクラス
public class EcHelper {

  // ログイン
  static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
  // ログアウト
  static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
  // 新規登録
  static final String USER_ADD_PAGE = "/WEB-INF/jsp/userAdd.jsp";
  // 新規登録完了
  static final String USER_ADD_RESULT_PAGE = "/WEB-INF/jsp/userAddResult.jsp";

  // ユーザー情報（参照と更新）
  static final String USER_INFO_PAGE = "/WEB-INF/jsp/userInfo.jsp";
  // ユーザー情報更新完了
  static final String USER_UPDATA_RESULT_PAGE = "/WEB-INF/jsp/userUpdateResult.jsp";
  // ユーザー購入履歴詳細
  static final String USER_BUY_HISTORY_PAGE = "/WEB-INF/jsp/buyHistory.jsp";

  // TOPページ
  static final String TOP_PAGE = "/WEB-INF/jsp/home.jsp";
  // 商品詳細ページ
  static final String ITEM_PAGE = "/WEB-INF/jsp/itemDetail.jsp";
  // 検索結果
  static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/itemSearch.jsp";

  // 買い物かごページ
  static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
  // 購入
  static final String BUY_PAGE = "/WEB-INF/jsp/buy.jsp";
  // 購入確認
  static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/buyConfirm.jsp";
  // 購入完了
  static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/buyResult.jsp";

  // クーポンくじページ
  static final String COUPON_KUJI_PAGE = "/WEB-INF/jsp/couponDrawing.jsp";
  // クーポンくじ当たりページ
  static final String COUPON_KUJI_WINNING_PAGE = "/WEB-INF/jsp/couponDrawingResultWinning.jsp";

  // クーポンくじはずれページ
  static final String COUPON_KUJI_OFF_PAGE = "/WEB-INF/jsp/couponDrawingResultOff.jsp";

  // 共通エラーページ
  static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";


  /**** 管理者専用ページ *****/
  // ユーザーマスタ一覧
  static final String USER_LIST_PAGE = "/WEB-INF/jsp/userList.jsp";
  // ユーザーマスタ削除
  static final String USER_DELETE_PAGE = "/WEB-INF/jsp/userDelete.jsp";

  // ユーザーマスタ削除完了
  static final String USER_DELETE_RESULT_PAGE = "/WEB-INF/jsp/userDeleteResult.jsp";
  // ユーザー新規登録画面、詳細情報参照＆更新画面は利用者と共通ページ

  // 商品マスタ一覧
  static final String ITEM_LIST_PAGE = "/WEB-INF/jsp/itemList.jsp";
  // 商品新規登録
  static final String ITEM_ADD_PAGE = "/WEB-INF/jsp/itemAdd.jsp";

  // 商品マスタ情報更新
  static final String ITEM_UPDATE_PAGE = "/WEB-INF/jsp/itemUpdate.jsp";
  // 商品マスタ情報削除
  static final String ITEM_DELETE_PAGE = "/WEB-INF/jsp/itemDelete.jsp";
  // 商品詳細参照画面は利用者と共通ページ

  /**** 管理者専用ページ *****/


  public static EcHelper getInstance() {
    return new EcHelper();
  }



  // 商品の合計金額を計算
  public static int getTotalItemPrice(ArrayList<ItemBeans> items) {
    int total = 0;
    for (ItemBeans item : items) {
      total += item.getItemPrice();
    }
    return total *= 1.1;
  }

  // 商品の消費税の計算
  public static int getTotalTaxPrice(ArrayList<ItemBeans> items) {
    int total = 0;
    double tax = 0.0;

    for (ItemBeans item : items) {
      total += item.getItemPrice();
    }

    total *= 1.1;
    tax = total - (total / 1.1);

    int taxPrice = (int) Math.round(tax);

    return taxPrice;
  }

  // セッションから指定データを取得（削除も一緒に行う）
  public static Object cutSessionAttribute(HttpSession session, String str) {
    Object test = session.getAttribute(str);
    session.removeAttribute(str);

    return test;
  }

  // ハッシュ関数（パスワードの暗号化処理）
  public static String encodePassword(String password) {

    // ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    // ハッシュ関数の種類(今回はMD5)
    String algorithm = "MD5";

    // ハッシュ生成処理
    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));

      String encodePassword = DatatypeConverter.printHexBinary(bytes);

      return encodePassword;

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return null;
    }
  }

}
