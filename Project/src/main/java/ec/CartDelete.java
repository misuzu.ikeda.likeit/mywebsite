package ec;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;

/**
 * カートに入っている商品の削除
 */
@WebServlet("/CartDelete")
public class CartDelete extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CartDelete() {
    super();
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      String[] deleteItemIdList = request.getParameterValues("delete_item_id_list");
      ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

      String cartActionMessage = "";
      if (deleteItemIdList != null) {
        // 削除対象の商品を削除
        for (String deleteItemId : deleteItemIdList) {
          for (ItemBeans cartInItem : cart) {
            if (cartInItem.getItemId() == Integer.parseInt(deleteItemId)) {
              cart.remove(cartInItem);
              break;
            }
          }
        }
        cartActionMessage = "削除しました";
      } else {
        cartActionMessage = "削除する商品が選択されていません";
      }
      request.setAttribute("cartActionMessage", cartActionMessage);
      request.getRequestDispatcher(EcHelper.CART_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
