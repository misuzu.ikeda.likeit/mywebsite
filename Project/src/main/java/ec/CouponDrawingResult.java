package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.CouponUserDAO;

/**
 * クーポンくじ結果ページ
 */
@WebServlet("/CouponDrawingResult")
public class CouponDrawingResult extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CouponDrawingResult() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    // クーポンくじの抽選と結果
    try {
      CouponUserDAO cud = new CouponUserDAO();

      // ログイン時に取得したユーザーIDをセッションから取得
      int userId = (int) session.getAttribute("userId");

      // くじ番号の抽選（0…当たり、1～9…はずれ）
      int kujiNum = new java.util.Random().nextInt(10);

      // 当たりの場合
      if (kujiNum == 0) {

        // DBに登録している「クーポンくじ」のクーポンID
        int couponId = 1;

        // 登録処理
        cud.insertCouponKujiWinning(userId, couponId);

        session.setAttribute("kujiDone", "kujiDone");

        request.getRequestDispatcher(EcHelper.COUPON_KUJI_WINNING_PAGE).forward(request, response);
        return;
      }

      // はずれの場合
      session.setAttribute("kujiDone", "kujiDone");
      request.getRequestDispatcher(EcHelper.COUPON_KUJI_OFF_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
