package ec;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDAO;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAdd extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAdd() {
    super();
  }


  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.getRequestDispatcher(EcHelper.USER_ADD_PAGE).forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      UserDAO ud = new UserDAO();

      String loginId = request.getParameter("login_id");
      String userName = request.getParameter("user_name");
      String postCode = request.getParameter("post_code");
      String address = request.getParameter("address");
      String birthDate = request.getParameter("birth_date");
      String password = request.getParameter("password");
      String confirmPassword = request.getParameter("confirm_password");

      // 入力されているパスワードが確認用と等しくない場合、loginIdが重複している場合
      if (!password.equals(confirmPassword) || UserDAO.existUser(loginId)) {

        // 入力内容を画面に表示するために、リクエストスコープに値をセットしておく
        request.setAttribute("inputLoginId", loginId);
        request.setAttribute("inputName", userName);
        request.setAttribute("inputPostCode", postCode);
        request.setAttribute("inputAddress", address);
        request.setAttribute("inputBirthdate", birthDate);

        // エラーメッセージ
        if (!password.equals(confirmPassword)) {
          // 入力されているパスワードが確認用と等しくない場合
          request.setAttribute("errMsg", "入力されているパスワードと確認用パスワードが違います");
        } else {
          // loginIdが重複している場合
          request.setAttribute("errMsg", "ほかのユーザーが使用中のログインIDです");
        }
        request.getRequestDispatcher(EcHelper.USER_ADD_PAGE).forward(request, response);
        return;
      }

      // 登録処理
      ud.insertUser(loginId, userName, postCode, address, birthDate, password);

      response.sendRedirect("UserAddResult");

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
