package ec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyBeans;
import beans.CouponMasterBeans;
import beans.DeliveryMethodBeans;
import beans.ItemBeans;
import dao.CouponUserDAO;
import dao.DeliveryMethodDAO;

/**
 * 購入
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Buy() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    try {

      Boolean isLogin =
          session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
              : false;
      ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

      if (!isLogin) {
        // Sessionにリターンページ情報を書き込む
        session.setAttribute("returnStrUrl", "Buy");
        // Login画面にリダイレクト
        response.sendRedirect("Login");

      } else if (cart.size() == 0) {
        request.setAttribute("cartActionMessage", "購入する商品がありません");
        request.getRequestDispatcher(EcHelper.CART_PAGE).forward(request, response);
      } else {
        // 配送方法をDBから取得
        ArrayList<DeliveryMethodBeans> dmbList = DeliveryMethodDAO.getAllDeliveryMethodBeans();
        request.setAttribute("dmbList", dmbList);

        // 所持しているクーポン情報をDBから取得
        int id = (int) session.getAttribute("userId");
        List<CouponMasterBeans> couponList = CouponUserDAO.getCouponListByUserId(id);
        request.setAttribute("couponList", couponList);

        request.getRequestDispatcher(EcHelper.BUY_PAGE).forward(request, response);
      }
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * 
   * 購入商品確認画面
   * 
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    try {
      BuyBeans bb = new BuyBeans();

      // 買い物かご
      ArrayList<ItemBeans> cartIBList = (ArrayList<ItemBeans>) session.getAttribute("cart");

      // 選択された配送方法IDを取得
      int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
      // 選択されたIDをもとに配送方法Beansを取得
      DeliveryMethodBeans userSelectDMB =
          DeliveryMethodDAO.getDeliveryMethodBeansByID(inputDeliveryMethodId);

      // 合計金額
      int totalPrice = EcHelper.getTotalItemPrice(cartIBList) + userSelectDMB.getPrice();
      // 消費税
      int taxPrice = EcHelper.getTotalTaxPrice(cartIBList);

      /* ====クーポン利用 start ==== */
      // 選択されたクーポンユーザーIDを取得
      int inputUserCouponId = Integer.parseInt(request.getParameter("use_coupon_id"));
      int couponDiscount = 0;

      if (inputUserCouponId > 0) {
        // 選択されたIDをもとにクーポン情報を取得
        CouponMasterBeans useSelectCMB =
            CouponUserDAO.getCouponDataByCouponUserId(inputUserCouponId);
        // 割引（円）の場合
        if (useSelectCMB.getDiscountYen() != 0) {
          totalPrice -= useSelectCMB.getDiscountYen();
          couponDiscount = useSelectCMB.getDiscountYen();

        } else if (useSelectCMB.getDiscountPercent() != 0) {
          // 割引（％）の場合
          totalPrice *= (100 - useSelectCMB.getDiscountPercent()) / 100;
          couponDiscount = (100 - useSelectCMB.getDiscountPercent()) / 100;
        } else if (useSelectCMB.isFreeShipping() == true) {
          // 送料無料の場合
          totalPrice -= userSelectDMB.getPrice();
          couponDiscount = userSelectDMB.getPrice();
        }
        bb.setCouponUserId(inputUserCouponId);
        bb.setCouponName(useSelectCMB.getCouponName());
        bb.setUseCouponDiscount(couponDiscount);
      }
      /* ====クーポン利用 end ==== */

      bb.setUserId((int) session.getAttribute("userId"));
      bb.setTotalPrice(totalPrice);
      bb.setDelivertMethodId(userSelectDMB.getDeliveryMethodId());
      bb.setDeliveryMethodName(userSelectDMB.getName());
      bb.setDeliveryMethodPrice(userSelectDMB.getPrice());

      // 購入確定で利用
      session.setAttribute("bb", bb);
      session.setAttribute("taxPrice", taxPrice);
      request.getRequestDispatcher(EcHelper.BUY_CONFIRM_PAGE).forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

}
