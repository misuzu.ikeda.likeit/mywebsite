package ec;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDAO;

/**
 * 商品マスタ一覧画面（※管理者専用）
 */
@WebServlet("/ItemList")
public class ItemList extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ItemList() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインセッション情報の確認
    HttpSession session = request.getSession();
    UserBeans loginCheck = (UserBeans) session.getAttribute("loginInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("Login");
      return;
    }

    try {
      // 商品一覧情報を取得
      ItemDAO item = new ItemDAO();
      List<ItemBeans> itemList = item.getAllItemData();

      // リクエストスコープに商品一覧情報をセット
      request.setAttribute("itemList", itemList);

      request.getRequestDispatcher(EcHelper.ITEM_LIST_PAGE).forward(request, response);

      // TODO ページング処理

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // 文字化け防止
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();

    try {
      ItemDAO item = new ItemDAO();

      // 検索条件取得
      String itemId = request.getParameter("item_id");
      String itemName = request.getParameter("item_name");
      String startPrice = request.getParameter("price_start");
      String endPrice = request.getParameter("price_end");

      // 検索処理
      List<ItemBeans> itemList = item.searchItem(itemId, itemName, startPrice, endPrice);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("itemList", itemList);
      request.setAttribute("itemId", itemId);
      request.setAttribute("itemName", itemName);
      request.setAttribute("startPrice", startPrice);
      request.setAttribute("endPrice", endPrice);

      // ユーザー一覧画面へ遷移
      request.getRequestDispatcher(EcHelper.ITEM_LIST_PAGE).forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
