package ec;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.BuyBeans;
import beans.BuyDetailBeans;
import beans.CouponMasterBeans;
import beans.ItemBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;
import dao.CouponUserDAO;

/**
 * 購入完了
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public BuyResult() {
    super();
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    try {
      // セッションからカート情報を取得
      ArrayList<ItemBeans> cart =
          (ArrayList<ItemBeans>) EcHelper.cutSessionAttribute(session, "cart");

      BuyBeans bb = (BuyBeans) EcHelper.cutSessionAttribute(session, "bb");

      // 購入情報を登録
      int buyId = BuyDAO.insertBuy(bb);
      // 購入詳細情報を購入情報IDに紐づけして登録
      for (ItemBeans cartInItem : cart) {
        BuyDetailBeans bdb = new BuyDetailBeans();
        bdb.setBuyId(buyId);
        bdb.setItemId(cartInItem.getItemId());
        BuyDetailDAO.insertBuyDetail(bdb);
      }

      // 使用済みクーポンの削除
      CouponUserDAO cud = new CouponUserDAO();
      cud.deleteUsedCoupon(bb.getCouponUserId());

      /* ====購入完了ページ表示用==== */
      BuyBeans resultBB = BuyDAO.getBuyBeansByBuyId(buyId);
      request.setAttribute("resultBB", resultBB);

      CouponMasterBeans cm = CouponUserDAO.getCouponDataByCouponUserId(bb.getCouponUserId());
      request.setAttribute("usedCouponData", cm);

      // 購入アイテム情報
      ArrayList<ItemBeans> buyIBList = BuyDetailDAO.getItemBeansListByBuyId(buyId);
      request.setAttribute("buyIBList", buyIBList);

      // 購入完了ページ
      request.getRequestDispatcher(EcHelper.BUY_RESULT_PAGE).forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("errorMessage", e.toString());
      response.sendRedirect("Error");
    }
  }
}
