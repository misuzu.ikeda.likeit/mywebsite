package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;

public class LoginCountDAO {

  /**
   * ログイン回数の登録
   * 
   * @param userId
   * @throws SQLException
   */
  public void insertLoginCount(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("INSERT INTO login_count(user_id, create_date) VALUES(?,now())");
      st.setInt(1, userId);

      st.executeUpdate();

      System.out.println("inserting loginCount has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  // ユーザーIDでログイン回数の取得
  public static double getLoginCount(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT COUNT(*) as loginCnt FROM login_count WHERE user_id = ?");
      st.setInt(1, userId);

      ResultSet rs = st.executeQuery();

      double loginCount = 0.0;

      while (rs.next()) {
        loginCount = Double.parseDouble(rs.getString("loginCnt"));
      }

      return loginCount;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }
}
