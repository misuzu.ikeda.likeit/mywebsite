package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.CouponMasterBeans;
import beans.CouponUserBeans;

public class CouponUserDAO {

  /**
   * ユーザーIDによるクーポンの種類ごとの枚数を取得
   * 
   * @param userId
   * @return クーポンの種類ごとの枚数
   * @throws SQLException
   */
  public List<CouponUserBeans> getCountCouponByUserId(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<CouponUserBeans> couponCntList = new ArrayList<CouponUserBeans>();

    try {
      con = DBManager.getConnection();

      // SELECTを実行し、結果表を取得
      st = con.prepareStatement("SELECT COUNT(*) AS coupon_cnt, coupon_name FROM coupon_user"
          + "   JOIN coupon_master ON coupon_user.coupon_id = coupon_master.coupon_id"
          + "   WHERE user_id = ? GROUP BY coupon_user.coupon_id");

      st.setInt(1, userId);
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        int couponCount = rs.getInt("coupon_cnt");
        String couponName = rs.getString("coupon_name");


        CouponUserBeans cub = new CouponUserBeans(couponCount, couponName);
        couponCntList.add(cub);
      }

      System.out.println("get couponCnt by userId completed");

      return couponCntList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * ユーザーIDに紐づく全クーポン情報の取得
   * 
   * @param userId
   * @return ユーザーIDに紐づく全クーポン情報
   * @throws SQLException
   */
  public static List<CouponMasterBeans> getCouponListByUserId(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<CouponMasterBeans> couponList = new ArrayList<CouponMasterBeans>();

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM coupon_user LEFT OUTER JOIN coupon_master "
          + "ON coupon_user.coupon_id = coupon_master.coupon_id "
          + "WHERE coupon_user.user_id = ? ORDER BY coupon_user.coupon_id");

      st.setInt(1, userId);
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        CouponMasterBeans cmb = new CouponMasterBeans();
        cmb.setCouponId(rs.getInt("coupon_id"));
        cmb.setCouponName(rs.getString("coupon_name"));
        cmb.setCouponLimit(rs.getTimestamp("coupon_limit"));
        cmb.setDiscountYen(rs.getInt("discount_yen"));
        cmb.setDiscountPercent(rs.getInt("discount_percent"));
        cmb.setFreeShipping(rs.getBoolean("free_shipping"));
        cmb.setCouponCode(rs.getString("coupon_cade"));
        cmb.setCouponUserId(rs.getInt("coupon_user_id"));
        couponList.add(cmb);
      }

      System.out.println("get couponList by userId completed");

      return couponList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * クーポンユーザーIDに紐づくクーポン情報を取得
   * 
   * @param couponUserId
   * @return
   * @throws SQLException
   */
  public static CouponMasterBeans getCouponDataByCouponUserId(int couponUserId)
      throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM coupon_user JOIN coupon_master "
          + "ON coupon_user.coupon_id = coupon_master.coupon_id WHERE coupon_user_id = ?");

      st.setInt(1, couponUserId);
      ResultSet rs = st.executeQuery();

      CouponMasterBeans cmb = new CouponMasterBeans();
      if (rs.next()) {
        cmb.setCouponId(rs.getInt("coupon_id"));
        cmb.setCouponName(rs.getString("coupon_name"));
        cmb.setCouponLimit(rs.getTimestamp("coupon_limit"));
        cmb.setDiscountYen(rs.getInt("discount_yen"));
        cmb.setDiscountPercent(rs.getInt("discount_percent"));
        cmb.setFreeShipping(rs.getBoolean("free_shipping"));
        cmb.setCouponCode(rs.getString("coupon_cade"));
        cmb.setCouponUserId(rs.getInt("coupon_user_id"));
      }

      System.out.println("searching CouponMasterBeans by couponUserId completed");

      return cmb;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * couponくじの当たった場合のクーポン登録
   * 
   * @param userId
   * @param couponId
   * @throws SQLException
   */
  public void insertCouponKujiWinning(int userId, int couponId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement(
          "INSERT INTO coupon_user(user_id, coupon_id, coupon_get_date) VALUES(?,?,now())");
      st.setInt(1, userId);
      st.setInt(2, couponId);

      st.executeUpdate();
      System.out.println("get couponId = 1 has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * 使用済みクーポンの削除
   * 
   * @param couponUserId
   * @throws SQLException
   */
  public void deleteUsedCoupon(int couponUserId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      st = con.prepareStatement("DELETE FROM coupon_user WHERE coupon_user_id =?");
      st.setInt(1, couponUserId);
      st.executeUpdate();

      System.out.println("delete used CouponData has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }
}
