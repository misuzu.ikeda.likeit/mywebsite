package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.UserBeans;
import ec.EcHelper;



public class UserDAO {

  /**
   * ログイン情報を取得
   * 
   * @param loginId
   * @param password
   * @return
   * @throws SQLException
   */
  public static UserBeans getLoginInfo(String loginId, String password) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
      st.setString(1, loginId);

      ResultSet rs = st.executeQuery();

      String loginIdData = null;
      String passwordData = null;
      boolean adminFlag = false;

      if (!rs.next()) {
        return null;
      }

      if (EcHelper.encodePassword(password).equals(rs.getString("password"))) {
        loginIdData = rs.getString("login_id");
        passwordData = rs.getString("password");
        adminFlag = rs.getBoolean("admin_flag");

        System.out.println("login succeeded");
      } else {
        return null;
      }

      return new UserBeans(loginIdData, passwordData, adminFlag);

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * ユーザーIDを取得
   * 
   * @param loginId
   * @param password
   * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
   * @throws SQLException
   */
  public static int getUserId(String loginId, String password) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
      st.setString(1, loginId);

      ResultSet rs = st.executeQuery();

      int userId = 0;
      while (rs.next()) {
        if (EcHelper.encodePassword(password).equals(rs.getString("password"))) {
          userId = rs.getInt("user_id");
          break;
        }
      }

      System.out.println("searching userId by loginId has been completed");

      return userId;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * ユーザー新規登録
   * 
   * @param loginId
   * @param userName
   * @param postCode
   * @param address
   * @param birthDate
   * @param password
   * @return なし
   * @throws SQLException
   */
  public void insertUser(String loginId, String userName, String postCode, String address,
      String birthDate, String password) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();
      st = con.prepareStatement(
          "INSERT INTO user(user_name, post_code, address, birth_date, login_id, password, create_date, update_date) VALUES(?,?,?,?,?,?,now(),now())");
      st.setString(1, userName);
      st.setString(2, postCode);
      st.setString(3, address);
      st.setString(4, birthDate);
      st.setString(5, loginId);
      st.setString(6, EcHelper.encodePassword(password));

      st.executeUpdate();
      System.out.println("inserting user has been completed");
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * ユーザー詳細情報の取得
   * 
   * @param id
   * @return idに紐づくユーザー情報
   */
  public UserBeans findById(int id) {
    Connection con = null;
    UserBeans user = null;
    PreparedStatement stmt = null;

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE user_id = ?";
      stmt = con.prepareStatement(sql);

      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      String userName = rs.getString("user_name");
      boolean adminFlag = rs.getBoolean("admin_flag");
      String postCode = rs.getString("post_code");
      String address = rs.getString("address");
      Date birthDate = rs.getDate("birth_date");
      String loginId = rs.getString("login_id");
      String password = rs.getString("password");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      user = new UserBeans(id, userName, adminFlag, postCode, address, birthDate, loginId, password,
          createDate, updateDate);

      System.out.println("searching userData by userId has been completed");

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        // コネクションインスタンスがnullでない場合、クローズ処理を実行
        if (con != null) {
          con.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    return user;
  }



  /**
   * ユーザー更新(password更新あり)
   * 
   * @param id
   * @param userName
   * @param postCode
   * @param address
   * @param password
   * @return なし
   * @throws SQLException
   */
  public void updateUser(int id, String userName, String postCode, String address, String password)
      throws SQLException {
    Connection con = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection();

      String sql =
          "UPDATE user SET user_name = ?, post_code = ?, address = ?, password = ?, update_date = now() WHERE user_id = ?";

      stmt = con.prepareStatement(sql);
      stmt.setString(1, userName);
      stmt.setString(2, postCode);
      stmt.setString(3, address);
      stmt.setString(4, EcHelper.encodePassword(password));
      stmt.setInt(5, id);

      // SQL実行
      stmt.executeUpdate();
      System.out.println("update userData has been completed");

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  /**
   * ユーザー更新（password更新なし）
   * 
   * @param id
   * @param userName
   * @param postCode
   * @param address
   * @return なし
   * @throws SQLException
   */
  public void updateUserExcludePassword(int id, String userName, String postCode, String address)
      throws SQLException {
    Connection con = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection();

      String sql =
          "UPDATE user SET user_name = ?, post_code = ?, address = ?, update_date = now() WHERE user_id = ?";

      stmt = con.prepareStatement(sql);
      stmt.setString(1, userName);
      stmt.setString(2, postCode);
      stmt.setString(3, address);
      stmt.setInt(4, id);

      // SQL実行
      stmt.executeUpdate();
      System.out.println("update userData has been completed");

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  /**
   * ログインIDが登録されているか確認する
   *
   * @param loginId
   * @return boolean
   */
  public static boolean existUser(String loginId) throws SQLException {
    Connection con = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ?";

      PreparedStatement pStmt = con.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        return true;
      }

      System.out.println("existUser check has been completed");
      return false;

    } catch (Exception e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }



  /**
   * 全てのユーザ情報を取得
   * 
   * @return 全ユーザー情報
   * @throws SQLException
   */
  public List<UserBeans> findAllUserData() throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<UserBeans> userList = new ArrayList<UserBeans>();

    try {
      con = DBManager.getConnection();

      // SELECTを実行し、結果表を取得
      st = con.prepareStatement("SELECT * FROM user WHERE admin_flag = 0");
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        int userId = rs.getInt("user_id");
        String userName = rs.getString("user_name");
        boolean adminFlag = rs.getBoolean("admin_flag");
        String postCode = rs.getString("post_code");
        String address = rs.getString("address");
        Date birthDate = rs.getDate("birth_date");
        String loginId = rs.getString("login_id");
        String password = rs.getString("password");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        UserBeans user = new UserBeans(userId, userName, adminFlag, postCode, address, birthDate,
            loginId, password, createDate, updateDate);

        userList.add(user);
      }
      System.out.println("get All userData completed");
      return userList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * ユーザー削除
   * 
   * @param id
   * @throws SQLException
   */
  public void deleteUser(int id) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      st = con.prepareStatement("DELETE FROM user WHERE user_id =?");
      st.setInt(1, id);
      st.executeUpdate();

      System.out.println("delete user has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }

  /**
   * ユーザー検索（ログインID、名前、住所、生年月日での検索）
   * 
   * @param loginId
   * @param userName
   * @param address
   * @param startDate
   * @param endDate
   * @return 検索条件に合うユーザー一覧
   * @throws SQLException
   */
  public List<UserBeans> searchUser(String loginId, String userName, String address,
      String startDate, String endDate) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<UserBeans> userList = new ArrayList<UserBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      /* SQL文の作成 start */
      StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE admin_flag = 0 ");
      List<String> parameters = new ArrayList<String>();

      // ログインIDが入力されていたらsqlに追加
      if (!(loginId.equals(""))) {
        sql.append(" AND login_id = ? ");
        parameters.add(loginId);
      }

      // 名前が入力されていたらsqlに追加
      if (!(userName.equals(""))) {
        sql.append(" AND user_name LIKE ? ");
        parameters.add("%" + userName + "%");
      }

      // 住所が入力されていたらsqlに追加
      if (!(address.equals(""))) {
        sql.append(" AND address LIKE ? ");
        parameters.add("%" + address + "%");
      }

      /* 生年月日の検索範囲 */
      // 始まりの日付が入力されていたらsqlに追加
      if (!(startDate.equals(""))) {
        sql.append(" AND birth_date >= ? ");
        parameters.add(startDate);
      }
      // 終わりの日付が入力されていたらsqlに追加
      if (!(endDate.equals(""))) {
        sql.append(" AND birth_date <= ? ");
        parameters.add(endDate);
      }
      /* SQL文の作成 end */

      // SELECTを実行し、結果表を取得
      st = con.prepareStatement(sql.toString());
      for (int i = 0; i < parameters.size(); i++) {
        st.setString(i + 1, parameters.get(i));
      }

      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        int resultUserId = rs.getInt("user_id");
        String resultUserName = rs.getString("user_name");
        boolean resultAdminFlag = rs.getBoolean("admin_flag");
        String resultPostCode = rs.getString("post_code");
        String resultAddress = rs.getString("address");
        Date resultBirthDate = rs.getDate("birth_date");
        String resultLoginId = rs.getString("login_id");
        String resultPassword = rs.getString("password");
        Timestamp resultCreateDate = rs.getTimestamp("create_date");
        Timestamp resultUpdateDate = rs.getTimestamp("update_date");
        UserBeans user = new UserBeans(resultUserId, resultUserName, resultAdminFlag,
            resultPostCode, resultAddress, resultBirthDate, resultLoginId, resultPassword,
            resultCreateDate, resultUpdateDate);

        userList.add(user);
      }

      System.out.println("search user has been completed");

      return userList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }

}
