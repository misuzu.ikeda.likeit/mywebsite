package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.BuyDetailBeans;
import beans.ItemBeans;

public class BuyDetailDAO {

  /**
   * 購入詳細登録処理
   * 
   * @param bdb (BuyDetailBeans)
   * @throws SQLException
   */
  public static void insertBuyDetail(BuyDetailBeans bdb) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)");
      st.setInt(1, bdb.getBuyId());
      st.setInt(2, bdb.getItemId());
      st.executeUpdate();
      System.out.println("inserting BuyDetail has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 購入IDによる購入情報検索
   * 
   * @param buyId
   * @return BuyDetailBeans
   * @throws SQLException
   */
  public ArrayList<BuyDetailBeans> getBuyBeansListByBuyId(int buyId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM buy_detail WHERE buy_id = ?");
      st.setInt(1, buyId);

      ResultSet rs = st.executeQuery();
      ArrayList<BuyDetailBeans> buyDetailList = new ArrayList<BuyDetailBeans>();

      while (rs.next()) {
        BuyDetailBeans bdb = new BuyDetailBeans();
        bdb.setId(rs.getInt("id"));
        bdb.setBuyId(rs.getInt("buy_id"));
        bdb.setItemId(rs.getInt("item_id"));
        buyDetailList.add(bdb);
      }

      System.out.println("searching BuyBeansList by BuyId has been completed");

      return buyDetailList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 購入IDによる購入詳細情報検索
   * 
   * @param buyId
   * @return buyDetailItemList ArrayList<ItemBeans> 購入詳細情報のデータを持つJavaBeansのリスト
   * @throws SQLException
   */
  public static ArrayList<ItemBeans> getItemBeansListByBuyId(int buyId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT item.item_id," + " item.item_name," + " item.item_price"
          + " FROM buy_detail" + " JOIN item" + " ON buy_detail.item_id = item.item_id"
          + " WHERE buy_detail.buy_id = ?");
      st.setInt(1, buyId);

      ResultSet rs = st.executeQuery();
      ArrayList<ItemBeans> buyDetailItemList = new ArrayList<ItemBeans>();

      while (rs.next()) {
        ItemBeans ib = new ItemBeans();
        ib.setItemId(rs.getInt("item_id"));
        ib.setItemName(rs.getString("item_name"));
        ib.setItemPrice(rs.getInt("item_price"));

        buyDetailItemList.add(ib);
      }

      System.out.println("searching ItemBeansList by BuyId has been completed");

      return buyDetailItemList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

}
