package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.DeliveryMethodBeans;

public class DeliveryMethodDAO {

  /**
   * DBに登録されている配送方法を取得
   * 
   * @return DeliveryMethodBeans
   * @throws SQLException
   */
  public static ArrayList<DeliveryMethodBeans> getAllDeliveryMethodBeans() throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM delivery_method");

      ResultSet rs = st.executeQuery();

      ArrayList<DeliveryMethodBeans> deliveryMethodBeansList = new ArrayList<DeliveryMethodBeans>();
      while (rs.next()) {
        DeliveryMethodBeans dmb = new DeliveryMethodBeans();
        dmb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
        dmb.setName(rs.getString("name"));
        dmb.setPrice(rs.getInt("price"));
        deliveryMethodBeansList.add(dmb);
      }

      System.out.println("searching all DeliveryMethodBeans has been completed");

      return deliveryMethodBeansList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }



  /**
   * 配送方法をIDをもとに取得
   * 
   * @param DeliveryMethodId
   * @return DeliveryMethodBeans
   * @throws SQLException
   */
  public static DeliveryMethodBeans getDeliveryMethodBeansByID(int DeliveryMethodId)
      throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM delivery_method WHERE delivery_method_id = ?");
      st.setInt(1, DeliveryMethodId);

      ResultSet rs = st.executeQuery();

      DeliveryMethodBeans dmb = new DeliveryMethodBeans();
      if (rs.next()) {
        dmb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
        dmb.setName(rs.getString("name"));
        dmb.setPrice(rs.getInt("price"));
      }

      System.out.println("searching DeliveryMethodBeans by DeliveryMethodID has been completed");

      return dmb;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }



}
