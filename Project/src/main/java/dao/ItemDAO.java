package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.ItemBeans;

public class ItemDAO {
  /**
   * ランダムで引数指定分のItemBeansを取得
   * 
   * @param limit 取得したい数
   * @return <ItemBeans>
   * @throws SQLException
   */
  public static ArrayList<ItemBeans> getRandItem(int limit) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM item ORDER BY RAND() LIMIT ? ");
      st.setInt(1, limit);

      ResultSet rs = st.executeQuery();

      ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

      while (rs.next()) {
        ItemBeans item = new ItemBeans();
        item.setItemId(rs.getInt("item_id"));
        item.setItemName(rs.getString("item_name"));
        item.setItemDetail(rs.getString("item_detail"));
        item.setItemPrice(rs.getInt("item_price"));
        item.setItemImage(rs.getString("Item_image"));
        itemList.add(item);
      }
      System.out.println("getAllItem completed");
      return itemList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品IDによる商品検索
   * 
   * @param itemId
   * @return
   * @throws SQLException
   */
  public static ItemBeans getItemByItemID(int itemId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM item WHERE item_id = ?");
      st.setInt(1, itemId);

      ResultSet rs = st.executeQuery();

      ItemBeans item = new ItemBeans();
      if (rs.next()) {
        item.setItemId(rs.getInt("item_id"));
        item.setItemName(rs.getString("item_name"));
        item.setItemDetail(rs.getString("item_detail"));
        item.setItemPrice(rs.getInt("item_price"));
        item.setItemImage(rs.getString("item_image"));
      }

      System.out.println("searching item by itemID has been completed");

      return item;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品検索
   * 
   * @param searchWord
   * @param pageNum
   * @param pageMaxItemCount
   * @return
   * @throws SQLException
   */
  public static ArrayList<ItemBeans> getItemsByItemName(String searchWord, int pageNum,
      int pageMaxItemCount) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      int startiItemNum = (pageNum - 1) * pageMaxItemCount;
      con = DBManager.getConnection();

      if (searchWord.length() == 0) {
        // 全検索
        st = con.prepareStatement("SELECT * FROM item ORDER BY item_id ASC LIMIT ?,? ");
        st.setInt(1, startiItemNum);
        st.setInt(2, pageMaxItemCount);
      } else {
        // 商品名検索
        st = con.prepareStatement(
            "SELECT * FROM item WHERE item_name LIKE ?  ORDER BY item_id ASC LIMIT ?,? ");
        st.setString(1, "%" + searchWord + "%");
        st.setInt(2, startiItemNum);
        st.setInt(3, pageMaxItemCount);
      }

      ResultSet rs = st.executeQuery();
      ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

      while (rs.next()) {
        ItemBeans item = new ItemBeans();
        item.setItemId(rs.getInt("item_id"));
        item.setItemName(rs.getString("item_name"));
        item.setItemDetail(rs.getString("item_detail"));
        item.setItemPrice(rs.getInt("item_price"));
        item.setItemImage(rs.getString("item_image"));
        itemList.add(item);
      }
      System.out.println("get Items by itemName has been completed");
      return itemList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品総数を取得
   * 
   * @param searchWord
   * @return
   * @throws SQLException
   */
  public static double getItemCount(String searchWord) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("select count(*) as cnt from item where item_name like ?");
      st.setString(1, "%" + searchWord + "%");
      ResultSet rs = st.executeQuery();
      double count = 0.0;
      while (rs.next()) {
        count = Double.parseDouble(rs.getString("cnt"));
      }
      return count;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品名が登録されているか確認する
   * 
   * @param itemName
   * @return boolean
   * @throws SQLException
   */
  public static boolean existItemName(String itemName) throws SQLException {
    Connection con = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection();

      String sql = "SELECT * FROM item WHERE item_name = ?";

      PreparedStatement pStmt = con.prepareStatement(sql);
      pStmt.setString(1, itemName);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        return true;
      }

      System.out.println("existItemName check has been completed");
      return false;

    } catch (Exception e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 全商品取得
   * 
   * @return
   * @throws SQLException
   */
  public List<ItemBeans> getAllItemData() throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
      con = DBManager.getConnection();

      // SELECTを実行し、結果表を取得
      st = con.prepareStatement("SELECT * FROM item");
      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        int itemId = rs.getInt("item_id");
        String itemName = rs.getString("item_name");
        String itemDetail = rs.getString("item_detail");
        int itemPrice = rs.getInt("item_price");
        String itemImage = rs.getString("item_image");
        ItemBeans item = new ItemBeans(itemId, itemName, itemDetail, itemPrice, itemImage);

        itemList.add(item);
      }

      System.out.println("get All itemData completed");
      return itemList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品新規登録
   * 
   * @param itemId
   * @param itemName
   * @param itemDetail
   * @param itemPrice
   * @param itemImage
   * @throws SQLException
   */
  public void insertItem(String itemName, String itemDetail, String itemPrice, String itemImage)
      throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement(
          "INSERT INTO item(item_name, item_detail, item_price, item_image) VALUES(?,?,?,?)");
      st.setString(1, itemName);
      st.setString(2, itemDetail);
      st.setString(3, itemPrice);
      st.setString(4, itemImage);

      st.executeUpdate();
      System.out.println("inserting item has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品情報の更新
   * 
   * @param id
   * @param itemName
   * @param itemDetail
   * @param itemPrice
   * @param itemImage
   * @throws SQLException
   */
  public void updateItem(int id, String itemName, String itemDetail, String itemPrice,
      String itemImage) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement(
          "UPDATE item SET item_name = ?, item_detail = ?, item_price = ?, item_image = ? WHERE item_id = ?");
      st.setString(1, itemName);
      st.setString(2, itemDetail);
      st.setString(3, itemPrice);
      st.setString(4, itemImage);
      st.setInt(5, id);

      st.executeUpdate();
      System.out.println("update itemData has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品情報の削除
   * 
   * @param id
   * @throws SQLException
   */
  public void deleteItem(int id) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;

    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("DELETE FROM item WHERE item_id =?");
      st.setInt(1, id);
      st.executeUpdate();

      System.out.println("delete itemData has been completed");

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 商品検索（商品ID、商品名、価格での検索）
   * 
   * @param itemId
   * @param itemName
   * @param startPrice
   * @param endPrice
   * @return 検索条件に合う商品一覧
   * @throws SQLException
   */
  public List<ItemBeans> searchItem(String itemId, String itemName, String startPrice,
      String endPrice) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      /* SQL文の作成 start */
      StringBuilder sql = new StringBuilder("SELECT * FROM item WHERE item_id IS NOT NULL");
      List<String> parameters = new ArrayList<String>();

      // 商品IDが入力されていたらsqlに追加
      if (!(itemId.equals(""))) {
        sql.append(" AND item_id = ? ");
        parameters.add(itemId);
      }

      // 商品名が入力されていたらsqlに追加
      if (!(itemName.equals(""))) {
        sql.append(" AND item_name LIKE ? ");
        parameters.add("%" + itemName + "%");
      }

      /* 価格の検索範囲 */
      // 始まりの値段が入力されていたらsqlに追加
      if (!(startPrice.equals(""))) {
        sql.append(" AND item_price >= ? ");
        parameters.add(startPrice);
      }
      // 終わりの値段が入力されていたらsqlに追加
      if (!(endPrice.equals(""))) {
        sql.append(" AND item_price <= ?");
        parameters.add(endPrice);
      }
      /* SQL文の作成 end */

      // SELECTを実行し、結果表を取得
      st = con.prepareStatement(sql.toString());
      for (int i = 0; i < parameters.size(); i++) {
        st.setString(i + 1, parameters.get(i));
      }

      ResultSet rs = st.executeQuery();

      while (rs.next()) {
        int resultItemId = rs.getInt("item_id");
        String resultItemName = rs.getString("item_name");
        String resultItemDetail = rs.getString("item_detail");
        int resultItemPrice = rs.getInt("item_price");
        String resultItemImage = rs.getString("item_image");
        ItemBeans item = new ItemBeans(resultItemId, resultItemName, resultItemDetail,
            resultItemPrice, resultItemImage);

        itemList.add(item);
      }

      System.out.println("search item has been completed");

      return itemList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      // データベース切断
      if (con != null) {
        con.close();
      }
    }
  }

}
