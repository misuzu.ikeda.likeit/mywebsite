package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import base.DBManager;
import beans.BuyBeans;

public class BuyDAO {


  /**
   * 購入情報登録処理
   * 
   * @param bb 購入情報
   * @return
   * @throws SQLException
   */
  public static int insertBuy(BuyBeans bb) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    int autoIncKey = -1;

    try {
      con = DBManager.getConnection();
      st = con.prepareStatement(
          "INSERT INTO buy(user_id, total_price, delivery_method_id, coupon_user_id, "
              + "buy_date, use_coupon_discount, coupon_id) VALUES(?, ?, ?, ?, now(), ?, ?)",
          Statement.RETURN_GENERATED_KEYS);
      st.setInt(1, bb.getUserId());
      st.setInt(2, bb.getTotalPrice());
      st.setInt(3, bb.getDelivertMethodId());
      st.setInt(4, bb.getCouponUserId());
      st.setInt(5, bb.getUseCouponDiscount());
      st.setInt(6, bb.getCouponId());
      st.executeUpdate();

      ResultSet rs = st.getGeneratedKeys();
      if (rs.next()) {
        autoIncKey = rs.getInt(1);
      }
      System.out.println("inserting buyDatas has been completed");

      return autoIncKey;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 購入IDによる購入情報検索
   * 
   * @param buyId
   * @return BuyBeans 購入情報のデータを持つJavaBeansのリスト
   * @throws SQLException
   */
  public static BuyBeans getBuyBeansByBuyId(int buyId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM ((( buy LEFT OUTER JOIN delivery_method"
          + "   ON buy.delivery_method_id = delivery_method.delivery_method_id)"
          + "   LEFT OUTER JOIN coupon_user AS cu ON buy.coupon_user_id = cu.coupon_user_id)"
          + "   LEFT OUTER JOIN coupon_master AS cm ON buy.coupon_id = cm.coupon_id)"
          + "   WHERE buy.buy_id = ?");
      st.setInt(1, buyId);

      ResultSet rs = st.executeQuery();

      BuyBeans bb = new BuyBeans();
      if (rs.next()) {
        bb.setBuyId(rs.getInt("buy_id"));
        bb.setTotalPrice(rs.getInt("total_price"));
        bb.setBuyDate(rs.getTimestamp("buy_date"));
        bb.setDelivertMethodId(rs.getInt("delivery_method_id"));
        bb.setUserId(rs.getInt("user_id"));
        bb.setDeliveryMethodPrice(rs.getInt("price"));
        bb.setDeliveryMethodName(rs.getString("name"));
        bb.setCouponUserId(rs.getInt("coupon_user_id"));
        bb.setCouponName(rs.getString("coupon_name"));
        bb.setUseCouponDiscount(rs.getInt("use_coupon_discount"));
      }

      System.out.println("searching BuyBeans by buyId has been completed");

      return bb;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  /**
   * 購入履歴の全取得
   * 
   * @param userId
   * @return BuyBeans
   * @throws SQLException
   */
  public ArrayList<BuyBeans> getBuyList(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM buy" + " JOIN delivery_method"
          + "   ON buy.delivery_method_id = delivery_method.delivery_method_id"
          + "   WHERE buy.user_id = ? ORDER BY buy.buy_id DESC");
      st.setInt(1, userId);

      ResultSet rs = st.executeQuery();
      ArrayList<BuyBeans> buyDataList = new ArrayList<BuyBeans>();

      while (rs.next()) {
        BuyBeans bb = new BuyBeans();
        bb.setBuyId(rs.getInt("buy_id"));
        bb.setUserId(rs.getInt("user_id"));
        bb.setTotalPrice(rs.getInt("total_price"));
        bb.setDelivertMethodId(rs.getInt("delivery_method_id"));
        bb.setBuyDate(rs.getTimestamp("buy_date"));
        bb.setDeliveryMethodName(rs.getString("name"));
        bb.setDeliveryMethodPrice(rs.getInt("price"));

        buyDataList.add(bb);
      }

      System.out.println("searching BuyDataBeansList by BuyId has been completed");
      return buyDataList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


}
