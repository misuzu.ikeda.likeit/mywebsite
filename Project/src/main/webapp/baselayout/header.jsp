<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />

<header>
	<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
		<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
			<li class="nav-item active"><a class="nav-link" href="Top">STONE
					STORE</a></li>
		</ul>
		<ul class="navbar-nav flex-row">

			<c:if test="${loginInfo.adminFlag == true }">
				<li class="nav-item"><a class="btn btn-outline-success btn-sm"
					href="ItemList">商品マスタ一覧</a> &nbsp; <a
					class="btn btn-outline-info btn-sm" href="UserList">ユーザー情報一覧</a>
					&nbsp;&nbsp;&nbsp;
			</c:if>
			<%
			boolean isLogin =
			    session.getAttribute("isLogin") != null ? (boolean) session.getAttribute("isLogin") : false;
			%>

			<%
			if (isLogin) {
			%>
			<c:if test="${loginInfo.adminFlag == false }">
				<li class="icon"><a href="UserInfo"> <span
						style="color: rgb(255, 255, 255);"> <svg
								xmlns="http://www.w3.org/2000/svg" width="25" height="25"
								fill="currentColor" class="bi bi-person-circle"
								viewBox="0 0 16 16">
  <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
  <path fill-rule="evenodd"
									d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
</svg>
					</span>
				</a></li>
			</c:if>
			<c:if test="${loginInfo.adminFlag == true }">
				<li class="icon"><a href="UserAdd"> <span
						style="color: rgb(255, 255, 255);"> <svg
								xmlns="http://www.w3.org/2000/svg" width="25" height="25"
								fill="currentColor" class="bi bi-person-plus"
								viewBox="0 0 16 16">
  <path
									d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
  <path fill-rule="evenodd"
									d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
</svg>
					</span>
				</a></li>
			</c:if>

			<%
			} else {
			%>
			<li class="icon"><a href="UserAdd"> <span
					style="color: rgb(255, 255, 255);"> <svg
							xmlns="http://www.w3.org/2000/svg" width="25" height="25"
							fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
  <path
								d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
  <path fill-rule="evenodd"
								d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
</svg>
				</span></a></li>
			<%
			}
			%>

			<li class="icon"><a href="Cart"> <span
					style="color: rgb(255, 255, 255);"> <svg
							xmlns="http://www.w3.org/2000/svg" width="25" height="25"
							fill="currentColor" class="bi bi-cart2" viewBox="0 0 16 16">
  <path
								d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z" />
</svg>
				</span>
			</a></li>

			<%
			if (isLogin) {
			%>
			<li class="icon"><a href="Logout"> <span
					style="color: rgb(255, 255, 255);"> <svg
							xmlns="http://www.w3.org/2000/svg" width="25" height="25"
							fill="currentColor" class="bi bi-box-arrow-in-right"
							viewBox="0 0 16 16">
  <path fill-rule="evenodd"
								d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z" />
  <path fill-rule="evenodd"
								d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
</svg>
				</span></a></li>
			<%
			} else {
			%>
			<li class="icon">
				<div class="bi-key">
					<a href="Login"> <span style="color: rgb(255, 255, 255);">
							<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
								fill="currentColor" class="bi bi-key" viewBox="0 0 16 16">
  <path
									d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z" />
  <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
</svg>
					</span></a>
				</div>
			</li>
			<%
			}
			%>
		</ul>
	</nav>

</header>

