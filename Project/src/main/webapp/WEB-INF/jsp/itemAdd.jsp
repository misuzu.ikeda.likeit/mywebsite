<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品新規登録（管理者専用ページ）</title>
<!-- fontColor.cssの読み込み -->
<link href="css/fontColor.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<h4 class="text-center">
			商品新規登録<span style="font-size: medium;">（管理者専用ページ）</span>
		</h4>
		<br>
		<div class="row">
			<div class="col-6 offset-3  mb-4">
				<!-- エラー処理 -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>

		<div class="card bg-light mb-3" id="cardColor">
			<div class="card-body">
				<form action="ItemAdd" method="POST">
					<h7 class="card-title">商品名</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_name"
							value="${inputItemName}" required>
					</p>
					<h7 class="card-title">商品詳細</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_detail"
							value="${inputItemDetail}" required>
					</p>
					<h7 class="card-title">税抜き価格（円）</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_price"
							value="${inputItemPrice}" required>
					</p>
					<h7 class="card-title">商品画像</h7>
					<p>
						<input type="file" class="form-control-file"
							id="exampleFormControlFile1" name="item_image"
							value="${inputItemImage}" required>
					</p>
					<br>
					<div class="text-center">
						<button type="submit" class="btn btn-info col-4" name="action">登録</button>
					</div>
				</form>
				<a href="ItemList" type="button" class="btn btn-link mb-3 btn-sm"
					style="float: right">キャンセル</a>
			</div>

		</div>
	</div>
</body>

</html>