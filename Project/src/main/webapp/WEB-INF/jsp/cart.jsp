<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>カート</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- imageSize.cssの読み込み -->
<link href="css/imageSize.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<!--TOPページに戻る-->
	<div class="container">
		<div class="row">
			<div class="col">
				<p class="text-right mb-3">
					<a class="btn btn-sm"
						style="color: rgb(133, 20, 20); background-color: rgba(255, 221, 191, 0.432);"
						href="Top">買い物を続ける</a>
				</p>
			</div>
		</div>
	</div>
	<!--カート内アイテム-->
	<div class="container">
		<div>
			<h4 class="text-center mb-3" style="color: rgb(150, 97, 0);">買い物かご</h4>
			<p class="text-center">${cartActionMessage}</p>
		</div>
		<br>
	</div>

	<div class="container">
		<form action="CartDelete" method="POST">
			<div class="row">
				<div class="col-4">
					<button class="btn btn-success btn-sm" style="float: left;"
						type="submit" name="action">
						商品の削除
						<svg xmlns="http://www.w3.org/2000/svg" width="19" height="18"
							fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                <path
								d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
                            </svg>
					</button>
				</div>
				<div class="col-4"></div>
				<div class="col-4">
					<a href="Buy" class="btn btn-info btn-sm" style="float: right;">
						レジに進む <svg xmlns="http://www.w3.org/2000/svg" width="19"
							height="19" fill="currentColor" class="bi bi-cash-coin"
							viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
								d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z" />
                                <path
								d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z" />
                                <path
								d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z" />
                                <path
								d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z" />
                            </svg>
					</a>
				</div>
			</div>
			<br>
			<div class="section">
				<div class="row">
					<c:forEach var="item" items="${cart}" varStatus="status">
						<div class="col-3 mb-4">
							<div class="card">
								<div class="card-image">
									<a href="ItemDetail?item_id=${item.itemId}"> <img
										src="item_image/${item.itemImage}" alt="${item.itemName}の画像"></a>
								</div>
								<br>
								<div class="card-content">
									<p class="text-center">
										<span class="card-title">${item.itemName}</span>
									</p>
									<p class="text-center">${item.formatPrice}円</p>
									<p class="col">
										<input class="check" type="checkbox" id="${status.index}"
											name="delete_item_id_list" value="${item.itemId}" />
										&nbsp;&nbsp;<label for="${status.index}">削除</label>
									</p>
								</div>
							</div>
						</div>
						<c:if test="${(status.index+1) % 4 == 0 }">
							<div class="row"></div>
							<br>
							<br>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</form>
	</div>
	<br>
	<br>
</body>

</html>