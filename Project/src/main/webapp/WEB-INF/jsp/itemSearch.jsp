<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品検索結果</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- imageSize.cssの読み込み -->
<link href="css/imageSize.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<br>
	<div class="container">
		<form action="ItemSearch">
			<div class="col-8 offset-2">
				<div class="input-group mb-2">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
								fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path
									d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                            </svg>
						</div>
					</div>
					<input type="text" name="search_word" class="form-control"
						id="inlineFormInputGroup" placeholder="検索ワードを入力">
				</div>
			</div>
		</form>
		<br>
	</div>

	<!--検索結果一覧-->
	<div class="container">
		<h4 class="text-center mb-2" style="color: rgb(14, 0, 136);">検索結果</h4>
		<p class="text-center" style="color: rgb(240, 48, 160);">${itemCount}件
			HITしました !</p>
		<br>
		<div class="section">
			<div class="row">
				<c:forEach var="item" items="${itemList}" varStatus="status">
					<div class="col-3 mb-4">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetail?item_id=${item.itemId}&page_num=${pageNum}"><img
									src="item_image/${item.itemImage}" alt="${item.itemName}の画像"></a>
							</div>
							<br>
							<div class="card-content">
								<p class="text-center">
									<span class="card-title">${item.itemName}</span>
								</p>
								<p class="text-center">${item.formatPrice}円</p>
							</div>
						</div>
					</div>
					<c:if test="${(status.index+1) % 4 == 0 }">
						<div class="row"></div>
						<br>
						<br>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
	<br>

	<!--ページング処理-->
	<div class="container">
		<ul class="pagination">
			<!-- 1ページ戻る -->
			<c:choose>
				<c:when test="${pageNum == 1}">
					<li class="disabled"><a></a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item"><a class="page-link"
						href="ItemSearch?search_word=${searchWord}&page_num=${pageNum - 1}"
						aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
					</a></li>
				</c:otherwise>
			</c:choose>

			<!-- ページインデックス -->
			<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}"
				end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1"
				varStatus="status">
				<li class="page-item"
					<c:if test="${pageNum == status.index }"> class="active" </c:if>>
					<a class="page-link"
					href="ItemSearch?search_word=${searchWord}&page_num=${status.index}">${status.index}</a>
				</li>
			</c:forEach>

			<!-- 1ページ送る -->
			<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a></a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item"><a class="page-link"
						href="ItemSearch?search_word=${searchWord}&page_num=${pageNum + 1}"
						aria-label="Previous"> <span aria-hidden="true">&raquo;</span>
					</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
	<br>
	<br>
	<br>
	<br>

</body>

</html>