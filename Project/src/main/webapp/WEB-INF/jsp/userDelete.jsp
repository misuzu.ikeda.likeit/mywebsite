<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー削除（管理者専用ページ）</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="text-center">
					ユーザー削除の確認<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
			</div>
		</div>
		<br>
		<div class="col s6 offset-s3">
			<div class="card grey lighten-5"
				style="background-color: rgb(255, 232, 236);">
				<div class="card-content">
					<br>
					<p class="text-center">
						（ログインID）<span class="mb-2" style="font-size: large">${user.loginId}</span>
					</p>
					<p class="text-center">
						<span class="mb-2" style="font-size: large"><b>${user.userName}さん</b></span>
						を削除しますか？
					</p>
					<br>

					<div class="row">
						<div class="col">
							<form action="UserDelete" method="POST">
								<button type="submit" class="btn btn-outline-danger col-6"
									style="float: right;" type="submit" name="id"
									value="${user.userId}">Yes</button>
							</form>
						</div>
						<div class="col">
							<a href="UserList" type="button"
								class="btn btn-outline-primary col-6">No</a>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>
</body>

</html>