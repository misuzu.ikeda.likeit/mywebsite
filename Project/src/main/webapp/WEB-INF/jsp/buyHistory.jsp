<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>購入履歴</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="row">
		<div class="col">
			<p>
			<h4 class="text-center mb-3">購入履歴の詳細</h4>
			</p>
		</div>
	</div>
	<!--  購入 -->
	<div class="container">
		<div class="row mt-3">
			<div class="col">
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(51, 51, 51); background-color: rgb(218, 198, 162);">
						<tr>
							<th>購入日時</th>
							<th>配送方法</th>
							<th>合計金額</th>
						</tr>
					</thead>
					<tbody class="text-center"
						style="background-color: rgb(255, 254, 253);">
						<tr>
							<td class="tdata">${buyHistory.formatBuyDate}</td>
							<td class="tdata">${buyHistory. deliveryMethodName}</td>
							<td class="tdata">${buyHistory.totalPrice}円</td>
						</tr>
					</tbody>
				</table>
				<br>
				<!-- 詳細 -->
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(66, 66, 66); background-color: rgb(255, 253, 230);">
						<tr>
							<th>商品名</th>
							<th>単価(税抜き)</th>
						</tr>
					</thead>
					<tbody class="text-center"
						style="background-color: rgb(255, 254, 253);">
						<c:forEach var="buyDetailItem" items="${buyDetailItem}">
							<tr>
								<td class="center">${buyDetailItem.itemName}</td>
								<td class="center">${buyDetailItem.itemPrice}円</td>
							</tr>
						</c:forEach>
						<tr>
							<td class="center">（内）消費税</td>
							<td class="center">${buyDetailTaxPrice}円</td>
						</tr>
						<tr>
							<td class="center">${buyHistory.deliveryMethodName}</td>
							<td class="center">${buyHistory.deliveryMethodPrice}円</td>
						</tr>
						<c:if test="${buyHistory.formatUseCouponDiscount != 0}">
							<tr>
								<td class="tdata">クーポン利用(${buyHistory.couponName})</td>
								<td class="tdata">－${buyHistory.formatUseCouponDiscount}円</td>
							</tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col">
				<p class="text-right mb-4">
					<a class="btn"
						style="color: rgb(122, 0, 0); background-color: rgb(250, 247, 242);"
						href="UserInfo">戻る</a>
				</p>
			</div>
		</div>
	</div>
</body>

</html>