<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品詳細</title>
<!-- fontColor.cssの読み込み -->
<link href="css/fontColor.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="text-center">商品詳細</h4>
			</div>
		</div>
		<br>

		<div class="card bg-light mb-3" id="cardColor">
			<div class="card-header">
				<b>${item.itemName}</b>
			</div>
			<div class="card-body">
				<div class="card" style="width: auto;">
					<img src="item_image/${item.itemImage}" class="card-img-top"
						alt="${item.itemName}の画像">
				</div>
				<br>
				<c:if test="${loginInfo.adminFlag == true }">
					<h7 class="card-title">商品ID</h7>
					<p class="card-text">${item.itemId}</p>
				</c:if>
				<h7 class="card-title">価格（税抜き）</h7>
				<p class="card-text">${item.formatPrice}円</p>
				<h7 class="card-title">商品説明</h7>
				<p class="card-text">${item.itemDetail}</p>
				<%
				boolean isLogin =
				    session.getAttribute("isLogin") != null ? (boolean) session.getAttribute("isLogin") : false;
				%>
				<c:if test="${loginInfo.adminFlag == false || isLogin == null}">
					<div class="text-center">
						<form action="CartAdd" method="POST">
							<input type="hidden" name="item_id" value="${item.itemId}">
							<button type="submit" class="btn btn-info" name="action">
								カートに追加&nbsp;
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
									fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16">
                                <path
										d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                                <path
										d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                            </svg>
							</button>
						</form>
					</div>
				</c:if>
			</div>
			<br>
		</div>

		<a href="javascript:void(0)" onclick="javascript:history.back()"
			style="float: right">戻る</a>
	</div>
	<br>
	<br>
</body>

</html>