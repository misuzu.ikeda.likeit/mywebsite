<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー情報詳細参照（管理者専用ページ）</title>
<!-- fontColor.cssの読み込み -->
<link href="css/fontColor.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<br>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="text-center">
					ユーザー情報詳細<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
			</div>
		</div>
		<br>
		<div class="card bg-light mb-3" id="cardColor">
			<div class="card-header">
				<b>池田美涼 さん</b>
			</div>
			<div class="card-body">
				<h7 class="card-title">ログインID</h7>
				<p class="card-text">ikeda</p>
				<h7 class="card-title">ユーザー名</h7>
				<p class="card-text">池田</p>
				<h7 class="card-title">郵便番号</h7>
				<p class="card-text">100-0000</p>
				<h7 class="card-title">住所</h7>
				<p class="card-text">東京都～～～～～～～～～～～～～～～～～～～～～～～～～～～～～～～～～～</p>
				<h7 class="card-title">生年月日</h7>
				<p class="card-text">1995年8月4日</p>
				<h7 class="card-title">登録日時</h7>
				<p class="card-text">1111年22月33日</p>
				<h7 class="card-title">更新日時</h7>
				<p class="card-text">1111年22月33日</p>
				<h7 class="card-title">ログイン回数</h7>
				<p class="card-text">1回</p>
				<h7 class="card-title">クーポン所持枚数</h7>
				<p class="card-text">0枚</p>
				<button type="button" class="btn btn-info" style="float: right">戻る</button>
			</div>
		</div>
	</div>
</body>

</html>