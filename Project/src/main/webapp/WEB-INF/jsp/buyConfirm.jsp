<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>購入確認</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container mb-2">
		<div class="row">
			<div class="col-6 offset-3">
				<h4 class="text-center" style="color: rgb(150, 97, 0);">購入確認</h4>
			</div>
		</div>
	</div>
	<div class="container mb-2">
		<p class="text-right">
			<a class="btn btn-outline-success btn-sm" href="Cart">カートに戻る</a>
		</p>
	</div>
	<div class="card-content" style="background-color: rgb(253, 249, 237);">
		<br>
		<div class="container">
			<div class="card-content">
				<form action="BuyResult" method="post">
					<div class="row mt-3">
						<div class="col">
							<div class="card grey lighten-5">
								<table class="table table-striped" id="textOverflow">
									<thead class="text-center"
										style="color: rgb(66, 66, 66); background-color: rgb(211, 202, 186);">
										<tr>
											<th>商品名</th>
											<th>単価(税抜き)</th>
										</tr>
									</thead>
									<tbody class="text-center"
										style="background-color: rgb(255, 254, 253);">
										<c:forEach var="cartInItem" items="${cart}">
											<tr>
												<td class="tdata">${cartInItem.itemName}</td>
												<td class="tdata">${cartInItem.formatPrice}円</td>
											</tr>
										</c:forEach>
										<tr>
											<td class="center">${bb.deliveryMethodName}</td>
											<td class="center">${bb.deliveryMethodPrice}円</td>
										</tr>
										<c:if test="${bb.couponUserId != null}">
											<tr>
												<c:if test="${bb.formatUseCouponDiscount == 0}">
													<td class="tdata">クーポン</td>
													<td>利用なし</td>
												</c:if>
												<c:if test="${bb.formatUseCouponDiscount != 0}">
													<td class="tdata">${bb.couponName}</td>
													<td class="tdata" style="color: red;">－${bb.formatUseCouponDiscount}円</td>
												</c:if>
											</tr>
										</c:if>
									</tbody>
								</table>
							</div>
							<br>
							<!--合計金額-->
							<div class="card grey lighten-5">
								<div style="background-color: rgb(255, 252, 247);">
									<div class="row mb-2" style="padding-top: 20px;">
										<div class="col-6">
											<p class="text-center">
												<b>合計金額</b>
											</p>
											<p class="text-center">（内）消費税</p>
										</div>
										<div class="col-6">
											<p class="text-center">
												<b>${bb.formatTotalPrice}円</b>
											</p>
											<p class="text-center">${taxPrice}円</p>
										</div>
									</div>
									<div class="col-6 offset-3">
										<p class="center-align">
											<button class="btn btn-info col s6 offset-s2" type="submit"
												name="action">購入</button>
										</p>
									</div>
									<br>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<br>
	</div>
	<br>
	<br>
</body>

</html>