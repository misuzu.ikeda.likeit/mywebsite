<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>クーポンくじ結果</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<h3 class="text-center mb-3" style="color: rgb(247, 173, 76);">
					<b>クーポンくじ</b>
				</h3>
			</div>
			<br> <br> <br> <br> <br>
		</div>
		<div class="col-8 offset-4 mb-2">
			<img src="icon/kuji_ken3_hazure.png">
		</div>
		<p class="text-center" style="color: rgb(0, 148, 197);">
			残念！<br> また挑戦してね！
		</p>
		<br> <a href="Top"
			class=" col-6 offset-3 btn
			btn-outline-info" type="button">TOPページへ
			&nbsp;</a>
	</div>
	<br>
	<br>
</body>

</html>