<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー削除の完了（管理者専用ページ）</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<h4 class="text-center">
					ユーザー削除<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
			</div>
		</div>
		<br>
		<div class="col s6 offset-s3">
			<div class="card grey lighten-5"
				style="background-color: rgb(242, 255, 221);">
				<div class="card-content">
					<br> <br>
					<p class="text-center">ユーザーの削除が完了しました</p>
					<br>
					<div class="col-6 offset-3">
						<p class="center-align">
							<a href="UserList" type="button"
								class="btn btn-outline-success col s8 offset-s2">ユーザー情報一覧へ</a>
						</p>
					</div>
					<br> <br>
				</div>
			</div>
		</div>
	</div>

</body>

</html>