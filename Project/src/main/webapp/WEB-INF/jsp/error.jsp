<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>エラー</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	</div>
	<div class="container">
		<br>
		<div class="col s6 offset-s3">
			<div class="card grey lighten-5"
				style="background-color: rgb(252, 233, 233);">
				<div class="card-content">
					<br>
					<h4 class="text-center mb-3">システムエラーが発生しました</h4>
					<h6 class="text-center mb-2" style="color: rgb(255, 0, 0);">${errorMessage}</h6>
					<div class="col-8 offset-2">
						<p class="center-align">
							<a href="Top" type="button" class="btn btn-link col s8 offset-s2">TOPページへ</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>