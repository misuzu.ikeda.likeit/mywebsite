<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品削除（管理者専用ページ）</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="text-center">
					登録商品の削除の確認<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
			</div>
		</div>
		<br>
		<form action="" method="POST">
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5"
					style="background-color: rgb(255, 227, 239);">
					<div class="card-content">
						<br>
						<p class="text-center">
							（商品名）<b>${item.itemName}</b> を削除しますか？
						</p>
						<br>
						<div class="row">
							<div class="col">
								<button type="submit" class="btn btn-outline-danger col-6"
									style="float: right;" type="submit" name="id"
									value="${item.itemId}">Yes</button>
							</div>
							<div class="col">
								<a href="ItemList" type="button"
									class="btn btn-outline-primary col-6">No</a>
							</div>
						</div>
						<br>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>

</html>