<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品マスタ一覧（管理者専用ページ）</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- 並べ替えのJSの読み込み -->
<script src="https://www.w3schools.com/lib/w3.js"></script>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- ユーザー情報一覧 -->
	<div class="container">
		<div class="row">
			<div class="col">
				<br>
				<p>
				<h4 class="text-center">
					商品マスタ一覧<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
				</p>
			</div>
		</div>

		<!-- 新規登録リンク -->
		<div class="row">
			<div class="col">
				<p class="text-right">
					<a class="btn btn-outline-success btn-sm" href="ItemAdd">商品
						新規登録</a>
				</p>
			</div>
		</div>

		<!-- 検索ボックス -->
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="text-center mb-3"
							style="color: rgba(38, 38, 172, 0.795);">商品検索</h5>
						<form action="ItemList" method="POST">
							<div class="form-group row">
								<label for="itemId" class="col-2 col-form-label text-center">商品ID</label>
								<div class="col-9">
									<input type="text" name="item_id" class="form-control"
										id="itemId" value="${itemId}">
								</div>
							</div>

							<div class="form-group row">
								<label for="itemName" class="col-2 col-form-label text-center">商品名</label>
								<div class="col-9">
									<input type="text" name="item_name" class="form-control"
										id="itemName" value="${itemName}" placeholder="部分一致で検索可">
								</div>
							</div>

							<div class="form-group row">
								<label for="itemPrice" class="col-2 col-form-label text-center">価格（税抜き）</label>
								<div class="col-9">
									<div class="row">
										<div class="col-5">
											<input type="text" name="price_start" id="startItemPrice"
												class="form-control" value="${startPrice}">
										</div>
										<div class="col-s2  text-left">円&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;～&nbsp;&nbsp;&nbsp;&nbsp;</div>
										<div class="col-5">
											<input type="text" name="price_end" id="endItemPrice"
												class="form-control" value="${endPrice}">
										</div>
										<div class="text-left">円</div>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit"
									class="btn btn-outline-primary form-submit">検索</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<br> <br>

		<!-- 商品一覧 -->
		<h5 class="text-center" style="color: rgba(38, 38, 172, 0.795);">商品一覧</h5>
		<div class="row mt-3">
			<div class="col">
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(252, 252, 252); background-color: rgb(43, 64, 146);">
						<tr>
							<th style="width: 15%"
								onclick="w3.sortHTML('#textOverflow','.item', 'td:nth-child(1)')">商品ID</th>
							<th
								onclick="w3.sortHTML('#textOverflow','.item', 'td:nth-child(2)')">商品名</th>
							<th style="width: 15%"
								onclick="w3.sortHTML('#textOverflow','.item', 'td:nth-child(3)')">価格</th>
							<th style="width: 20%"
								onclick="w3.sortHTML('#textOverflow','.item', 'td:nth-child(4)')"></th>
						</tr>
					</thead>
					<tbody class="text-center">
						<c:forEach var="item" items="${itemList}">
							<tr class="item">
								<td class="tdata">${item.itemId}</td>
								<td class="tdata text-left">${item.itemName}</td>
								<td class="tdata">${item.formatPrice}円</td>

								<td><a class="btn btn-info btn-sm"
									href="ItemDetail?item_id=${item.itemId}">詳細</a>&nbsp;&nbsp;<a
									class="btn btn-success btn-sm"
									href="ItemUpdate?item_id=${item.itemId}">更新</a>&nbsp;&nbsp;<a
									class="btn btn-warning btn-sm"
									href="ItemDelete?item_id=${item.itemId}">削除</a>&nbsp;&nbsp;</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

</html>