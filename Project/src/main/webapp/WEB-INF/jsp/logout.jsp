<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ログアウト完了</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<div class="row">
		<div class="col s8 offset-s2">
			<div class="card grey lighten-5">
				<div class="card-content">
					<p>
					<h4 class="text-center">ログアウトしました</h4>
					</p>
				</div>
				<div class="row">
					<div class="col s12">
						<p class="center-align">
							<a href="Top" type="button" class="btn btn-link col s8 offset-s2">TOPページへ
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>

</html>