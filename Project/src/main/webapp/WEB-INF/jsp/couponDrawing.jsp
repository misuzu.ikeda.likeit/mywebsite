<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>クーポンくじ</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<h3 class="text-center mb-3" style="color: rgb(75, 206, 162);">
					<b>クーポンくじ</b>
				</h3>
			</div>
			<p class="col-12 text-center" style="color: rgb(255, 150, 13);">
				10回ログインありがとう！<br> くじを引いて 100円OFFクーポンを当てよう！
			</p>
			<br> <br> <br> <br> <br>
			<div class="col-6 offset-4 mb-2">
				<a href="CouponDrawingResult"><img src="icon/kuji.png"></a>
			</div>
		</div>
		<p class="text-center">
			<span style="font-size: small;">イラストをクリックして結果を確認しよう！</span>
		</p>
	</div>
	<br>
	<br>
</body>

</html>