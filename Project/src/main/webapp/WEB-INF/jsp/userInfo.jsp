<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー情報</title>
<!-- fontColor.cssの読み込み -->
<link href="css/fontColor.css" rel="stylesheet" type="text/css" />
<!--郵便番号から住所検索のためのソース-->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-2">
				<h4 class="text-center">ユーザー情報</h4>
			</div>
		</div>
		<c:if test="${loginInfo.adminFlag == false}">
			<div class="container">
				<p class="text-right">
					<a class="btn btn-outline-info btn-sm" href="Top">TOPページへ</a>
				</p>
			</div>
		</c:if>
		<div class="row">
			<div class="col-6 offset-3  mb-4">
				<!-- エラー処理 -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>
	</div>

	<!-- ユーザー更新ボックス -->
	<div class="container">
		<div class="card bg-light mb-3" id="cardColor">
			<div class="card-header">
				<b>${userData.userName} さん</b>
			</div>
			<form action="UserInfo" method="POST">
				<div class="card-body">
					<h7 class="card-title">ログインID</h7>
					<p class="card-text">${userData.loginId}</p>
					<h7 class="card-title">ユーザー名</h7>
					<div>
						<input type="text" name="user_name" class="form-control mb-3"
							value="${userData.userName}" required>
					</div>
					<h7 class="card-title">住所</h7>
					<div>
						<!-- 郵便番号入力(7桁) -->
						<input type="text" name="post_code" value="${userData.postCode}"
							onKeyUp="AjaxZip3.zip2addr('post_code', '', 'address', 'address');"
							class="form-control col-6 mb-2" placeholder="〒" required />
						<!-- 住所入力(都道府県+以降の住所) -->
						<input type="text" name="address" value="${userData.address}"
							class="form-control mb-3" required />
					</div>

					<h7 class="card-title">パスワード</h7>
					<div>
						<input type="password" name="password" class="form-control mb-3">
					</div>
					<h7 class="card-title">パスワード（確認用）</h7>
					<div>
						<input type="password" name="confirm_password"
							class="form-control mb-3">
					</div>
					<h7 class="card-title">生年月日</h7>
					<p class="card-text">
						<fmt:formatDate value="${userData.birthDate}"
							pattern="yyyy年 MM月 dd日" />
					</p>
					<h7 class="card-title">ログイン回数</h7>
					<p class="card-text">${loginCount}回</p>

					<h7 class="card-title">所持しているクーポン</h7>
					<c:forEach var="coupon" items="${couponCountList}">
						<p class="card-text">${coupon.couponName}：&nbsp;
							${coupon.couponCnt}枚</p>
					</c:forEach>

					<c:if test="${loginInfo.adminFlag == true}">
						<h7 class="card-title">登録日時</h7>
						<p class="card-text">
							<fmt:formatDate value="${userData.createDate}"
								pattern="yyyy年MM月dd日HH時mm分" />
						</p>
						<h7 class="card-title">更新日時</h7>
						<p class="card-text">
							<fmt:formatDate value="${userData.updateDate}"
								pattern="yyyy年MM月dd日HH時mm分" />
						</p>
					</c:if>

					<button type="submit" class="btn btn-info mb-3"
						style="float: right">更新</button>
				</div>
			</form>
		</div>
	</div>
	<br>

	<!-- 購入履歴一覧 -->
	<div class="container">
		<h5 class="text-center" style="color: rgba(38, 38, 172, 0.795);">購入履歴</h5>
		<div class="row">
			<div class="col">
				<table class="table table-striped">
					<thead class="text-center"
						style="color: rgb(252, 252, 252); background-color: rgb(43, 64, 146);">
						<tr>
							<th class="center" style="width: 30%;">購入日時</th>
							<th class="center">配送方法</th>
							<th class="center" style="width: 20%;">合計金額</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="text-center">
						<c:forEach var="buyDataList" items="${buyDataList}">
							<tr>
								<td><fmt:formatDate value="${buyDataList.buyDate}"
										pattern="yyyy年MM月dd日HH時mm分" /></td>
								<td>${buyDataList.deliveryMethodName}</td>
								<td>${buyDataList.totalPrice}円</td>
								<td><a class="btn btn-outline-secondary btn-sm"
									href="BuyHistoryDetail?buy_id=${buyDataList.buyId}">詳細</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<br>
</body>

</html>