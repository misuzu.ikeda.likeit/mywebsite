<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>STONE STORE</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- imageSize.cssの読み込み -->
<link href="css/imageSize.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>


	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-3">
				<h4 class="text-center">STONE STORE</h4>
			</div>
		</div>
		<br>
	</div>

	<div class="container">
		<c:if
			test="${loginInfo != null && kujiDone == null && loginCount % 10 == 0 }">
			<div class="text-center mb-5">
				<a href="CouponDrawing"><img src="icon/kuji.png" alt="クーポンくじ"
					width="180" height="150"></a>
				<p class="text-center mb-4">↑画像をクリックでくじページへ</p>
			</div>
		</c:if>
	</div>
	<div class="container">
		<form action="ItemSearch">
			<div class="col-8 offset-2">
				<div class="input-group mb-2">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
								fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path
									d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                            </svg>
						</div>
					</div>
					<input type="text" name="search_word" class="form-control"
						id="inlineFormInputGroup" placeholder="検索ワードを入力">
				</div>
			</div>
		</form>
	</div>
	<br>
	<br>
	<div class="container">
		<!--   おすすめ商品   -->
		<h5 class="text-center" style="color: rgb(6, 104, 150);">おすすめアイテム</h5>
		<br>
		<div class="section">
			<div class="row">
				<c:forEach var="item" items="${itemList}">
					<div class="col-3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetail?item_id=${item.itemId }"> <img
									src="item_image/${item.itemImage }"></a>
							</div>
							<br>
							<div class="card-content">
								<p class="text-center">
									<span class="card-title">${item.itemName}</span>
								</p>
								<p class="text-center">${item.formatPrice}円</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<br>
	<br>
</body>

</html>