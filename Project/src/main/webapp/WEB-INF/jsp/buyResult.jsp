<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>購入完了</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div>
			<h4 class="text-center mb-3" style="color: rgb(150, 97, 0);">購入が完了しました</h4>
			<p class="text-center mb-4" style="color: rgb(150, 97, 0);">ご利用ありがとうございました</p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<p class="text-right mb-4">
					<a class="btn btn-sm"
						style="color: rgb(133, 20, 20); background-color: rgba(233, 214, 197, 0.432);"
						href="Top">TOPページへ</a>
				</p>
			</div>
		</div>
	</div>
	<h6 class="text-center" style="color: rgb(177, 122, 22);">購入詳細</h6>
	<!--  購入 -->
	<div class="container">
		<div class="row mt-3">
			<div class="col">
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(51, 51, 51); background-color: rgb(218, 198, 162);">
						<tr>
							<th>購入日時</th>
							<th>配送方法</th>
							<th>合計金額</th>
						</tr>
					</thead>
					<tbody class="text-center"
						style="background-color: rgb(255, 254, 253);">
						<tr>
							<td class="tdata">${resultBB.formatBuyDate}</td>
							<td class="tdata">${resultBB.deliveryMethodName}</td>
							<td class="tdata">${resultBB.formatTotalPrice}円</td>
						</tr>
					</tbody>
				</table>
				<br>
				<!-- 詳細 -->
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(66, 66, 66); background-color: rgb(255, 253, 230);">
						<tr>
							<th>商品名</th>
							<th>小計</th>
						</tr>
					</thead>
					<tbody class="text-center"
						style="background-color: rgb(255, 254, 253);">

						<c:forEach var="buyIB" items="${buyIBList}">
							<tr>
								<td class="center">${buyIB.itemName}</td>
								<td class="center">${buyIB.formatPrice}円（税抜き）</td>
							</tr>
						</c:forEach>
						<tr>
							<td class="center">消費税</td>
							<td class="center">${taxPrice}円</td>
						</tr>
						<tr>
							<td class="center">${resultBB.deliveryMethodName}</td>
							<td class="center">${resultBB.deliveryMethodPrice}円</td>
						</tr>
						<c:if test="${resultBB.couponUserId != null}">
							<tr>
								<c:if test="${resultBB.useCouponDiscount == 0}">
									<td class="tdata">クーポン</td>
									<td>利用なし</td>
								</c:if>
								<c:if test="${resultBB.useCouponDiscount != 0}">
									<td class="tdata">${usedCouponData.couponName}</td>
									<td class="tdata" style="color: red;">－${resultBB.useCouponDiscount}円</td>
								</c:if>
							</tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
</body>

</html>