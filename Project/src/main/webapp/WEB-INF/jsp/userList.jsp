<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー情報一覧（管理者専用ページ）</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- ユーザー情報一覧 -->
	<div class="container">
		<div class="row">
			<div class="col">
				<p>
				<h4 class="text-center">
					ユーザー情報一覧<span style="font-size: medium;">（管理者専用ページ）</span>
				</h4>
				</p>
			</div>
		</div>

		<!-- 検索ボックス -->

		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="text-center mb-3"
							style="color: rgba(38, 38, 172, 0.795);">ユーザー検索</h5>
						<form action="UserList" method="POST">
							<div class="form-group row">
								<label for="loginId" class="col-4 col-form-label">ログインID</label>
								<div class="col-8">
									<input type="text" name="user_loginId" class="form-control"
										id="loginId" value="${loginId}">
								</div>
							</div>

							<div class="form-group row">
								<label for="userName" class="col-4 col-form-label">ユーザ名</label>
								<div class="col-8">
									<input type="text" name="user_name" class="form-control"
										id="userName" value="${userName}" placeholder="部分一致で検索可">
								</div>
							</div>

							<div class="form-group row">
								<label for="" class="col-4 col-form-label">住所</label>
								<div class="col-8">
									<input type="text" name="address" class="form-control"
										id="address" value="${address}" placeholder="部分一致で検索可">
								</div>
							</div>

							<div class="form-group row">
								<label for="birthDate" class="col-4 col-form-label">生年月日</label>
								<div class="col-8">
									<div class="row">
										<div class="col-5">
											<input type="date" name="date_start" id="startBirthDate"
												class="form-control" value="${startDate}">
										</div>
										<div class="col-2 text-center">~</div>
										<div class="col-5">
											<input type="date" name="date_end" id="endBirthDate"
												class="form-control" value="${endDate}">
										</div>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit"
									class="btn btn-outline-primary form-submit">検索</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>


		<br> <br>

		<!-- 検索結果一覧 -->
		<h5 class="text-center" style="color: rgba(38, 38, 172, 0.795);">ユーザー情報一覧</h5>
		<div class="row mt-3">
			<div class="col">
				<table class="table table-striped" id="textOverflow">
					<thead class="text-center"
						style="color: rgb(252, 252, 252); background-color: rgb(43, 64, 146);">
						<tr>
							<th style="width: 15%">ログインID</th>
							<th style="width: 15%">氏名</th>
							<th>住所</th>
							<th style="width: 15%">生年月日</th>
							<th style="width: 20%"></th>
						</tr>
					</thead>
					<tbody class="text-center">
						<c:forEach var="user" items="${userList}">
							<tr>
								<td class="tdata">${user.loginId}</td>
								<td class="tdata">${user.userName}</td>
								<td class="tdata text-left">${user.address}</td>
								<td class="tdata"><fmt:formatDate value="${user.birthDate}"
										pattern="yyyy年MM月dd日" /></td>
								<td><a class="btn btn-info btn-sm"
									href="isAdminOnryUserData?id=${user.userId}">詳細/更新</a>&nbsp;&nbsp;&nbsp;
									<a class="btn btn-success btn-sm"
									href="UserDelete?id=${user.userId}">削除</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<br>
	<br>
</body>

</html>