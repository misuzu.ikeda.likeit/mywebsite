<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>購入</title>
<!-- textOverflow.cssの読み込み -->
<link href="css/textOverflow.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<h4 class="text-center" style="color: rgb(150, 97, 0);">カートアイテム</h4>
			</div>
		</div>
	</div>

	<div class="container mb-2">
		<p class="text-right">
			<a class="btn btn-outline-success btn-sm" href="Cart">カートに戻る</a>
		</p>
	</div>
	<div class="card-content" style="background-color: rgb(253, 249, 237);">
		<br>
		<div class="container">
			<div class="card-content">
				<form action="Buy" method="POST">
					<div class="row mt-3">
						<div class="col">
							<div class="card grey lighten-5">
								<table class="table table-striped" id="textOverflow">
									<thead class="text-center"
										style="color: rgb(66, 66, 66); background-color: rgb(211, 202, 186);">
										<tr>
											<th>商品名</th>
											<th style="width: 40%">単価(税抜き)</th>
										</tr>
									</thead>
									<tbody class="text-center"
										style="background-color: rgb(255, 254, 253);">
										<c:forEach var="cartInItem" items="${cart}">
											<tr>
												<td class="tdata">${cartInItem.itemName}</td>
												<td class="tdata">${cartInItem.formatPrice}円</td>
											</tr>
										</c:forEach>
									</tbody>
									<thead class="text-center"
										style="color: rgb(66, 66, 66); background-color: rgb(211, 202, 186);">
										<tr>
											<th>オプション</th>
											<th></th>
										</tr>
									</thead>
									<tr>
										<td class="tdata text-center">配送方法を選択</td>
										<td class="tdata">
											<div class="text-center">
												<select class="text-center" name="delivery_method_id">
													<c:forEach var="dmb" items="${dmbList}">
														<option value="${dmb.deliveryMethodId}">${dmb.name}</option>
													</c:forEach>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td class="tdata text-center">クーポンを選択</td>
										<td class="tdata">
											<div class="text-center">
												<select class="text-center" name="use_coupon_id">
													<option value="0">利用しない</option>
													<c:forEach var="coupon" items="${couponList}">
														<option value="${coupon.couponUserId}">${coupon.couponName}</option>
													</c:forEach>
												</select>
											</div>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
							<br>
							<!--購入ボタン-->
							<div class="card grey lighten-5">
								<div style="background-color: rgb(255, 252, 247);">
									<div style="padding-top: 20px;">
										<div class="text-center">
											<p class="text-center">
												<b>以上のアイテムを購入しますか？</b>
											</p>
										</div>
									</div>
									<div class="col-6 offset-3">
										<p class="center-align">
											<button class="btn btn-info col s6 offset-s2" type="submit"
												name="action">購入確認</button>
										</p>
									</div>
									<br>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<br>
	</div>
	<br>
	<br>
</body>

</html>