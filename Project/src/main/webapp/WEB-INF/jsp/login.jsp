<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ログイン</title>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- ログインフォーム -->
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-5">
				<h4 class="text-center">ログイン画面</h4>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3 mb-5">
				<!-- エラー処理 -->
				<c:if test="${loginErrorMessage != null}">
					<div class="alert alert-danger text-center" role="alert">${loginErrorMessage}</div>
				</c:if>

			</div>
		</div>

		<form action="Login" method="post">
			<div class="row">
				<div class="col-8 offset-2">
					<div class="form-group row">
						<label for="inputLoginId" class="col-3 col-form-label">ログインID</label>
						<div class="col-9">
							<input type="text" name="login_id" value="${inputLoginId}"
								class="form-control" autofocus required>
						</div>
					</div>

					<div class="form-group row mb-5">
						<label for="inputPassword" class="col-3 col-form-label">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" class="form-control"
								autofocus required>
						</div>
					</div>

					<button class="col-6 offset-3 btn btn-outline-secondary"
						type="submit" name="action">ログイン</button>
				</div>
			</div>
		</form>
	</div>
</body>

</html>