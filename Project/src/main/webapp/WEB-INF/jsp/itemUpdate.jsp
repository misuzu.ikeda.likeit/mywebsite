<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>商品情報更新（管理者専用ページ）</title>
<!-- fontColor.cssの読み込み -->
<link href="css/fontColor.css" rel="stylesheet" type="text/css" />
<!--郵便番号から住所検索のためのソース-->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>

	<div class="container">
		<h4 class="text-center">
			商品情報の更新<span style="font-size: medium;">（管理者専用ページ）</span>
		</h4>
		<br>
		<div class="row">
			<div class="col-6 offset-3  mb-4">
				<!-- エラー処理 -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>
	</div>

	<!-- ユーザー更新ボックス -->
	<div class="container">
		<div class="card bg-light mb-3" id="cardColor">
			<div class="card-body">
				<form action="ItemUpdate" method="POST">
					<h7 class="card-title">商品ID</h7>
					<input type="hidden" name="id" value="${item.itemId}">
					<p class="card-text">${item.itemId}</p>
					<h7 class="card-title">商品名</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_name"
							value="${item.itemName}" required>
					</p>
					<h7 class="card-title">商品詳細</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_detail"
							value="${item.itemDetail}" required>
					</p>
					<h7 class="card-title">税抜き価格（円）</h7>
					<p>
						<input type="text" class="form-control mb-3" name="item_price"
							value="${item.itemPrice}" required>
					</p>
					<div style="padding-bottom: 5px;">
						<h7 class="card-title">商品画像<span
							style="font-size: small; color: black;">（現在使用しているファイル名：${item.itemImage}）</span></h7>
					</div>

					<p>
						<input type="file" class="form-control-file"
							id="exampleFormControlFile1" name="item_image"
							value="${item.itemImage}" required>
					</p>

					<br>
					<div class="text-center">
						<button type="submit" class="btn btn-info col-4" name="action">更新</button>
					</div>
				</form>
				<a href="ItemList" type="button" class="btn btn-link mb-3 btn-sm"
					style="float: right">キャンセル </a>
			</div>
		</div>
	</div>
	<br>
</body>

</html>