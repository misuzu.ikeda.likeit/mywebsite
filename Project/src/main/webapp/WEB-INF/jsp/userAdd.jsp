<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>ユーザー新規登録</title>
<!--郵便番号から住所検索のためのソース-->
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<h4 class="text-center">ユーザー新規登録</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-6 offset-3  mb-4">
				<!-- エラー処理 -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger text-center" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>

		<div class="col s6 offset-s3">
			<div class="card grey lighten-5"
				style="background-color: rgb(255, 255, 234);">
				<div class="card-content">
					<br> <br> <br>
					<form action="UserAdd" method="POST">
						<div class="col-10 offset-1" style="margin-bottom: 15px">
							<div>
								<input type="text" name="login_id" value="${inputLoginId}"
									class="form-control mb-3" placeholder="ログインID" required>
							</div>
							<div>
								<input type="text" name="user_name" value="${inputName}"
									class="form-control mb-3" placeholder="名前" required>
							</div>
							<div>
								<!-- 郵便番号入力 -->
								<input type="text" name="post_code" value="${inputPostCode}"
									onKeyUp="AjaxZip3.zip2addr('post_code', '', 'address', 'address');"
									class="form-control col-6 mb-2" placeholder="〒" required />
								<!-- 住所入力(都道府県+以降の住所) -->
								<input type="text" name="address" value="${inputAddress}"
									class="form-control mb-3" placeholder="住所" required />
							</div>
							<div>
								<input type="date" name="birth_date" value="${inputBirthdate}"
									class="form-control mb-3" placeholder="生年月日" required>
							</div>
							<div>
								<input type="password" name="password" class="form-control mb-3"
									placeholder="パスワード" required>
							</div>
							<div>
								<input type="password" name="confirm_password"
									class="form-control mb-3" placeholder="パスワード（確認用）" required>
							</div>
						</div>
						<div class="col-8 offset-2">
							<p class="center-align">
								<button class="btn btn-outline-success col s8 offset-s2"
									type="submit" name="action">登録</button>
							</p>
						</div>
					</form>
					<a href="Top" type="button" class="btn btn-link mb-3"
						style="float: right">キャンセル</a> <br> <br> <br>
				</div>
			</div>
		</div>
	</div>
</body>

</html>